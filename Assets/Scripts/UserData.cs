﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class UserData : MonoBehaviour
{
	[SerializeField] private ProgramController programController;
	[SerializeField] private ProfileController profileController;
	[SerializeField] private ForegroundDataManager foregroundDataManager;
	[SerializeField] private SpeechToTextAnswer speechToText;
	[SerializeField] private IntroPageController introController;
	[SerializeField] private OrganizerController organizerController;

	[Header("Podometer")]
	[SerializeField] private PodometroPlugins podometer;
	[SerializeField] private Slider podometerSlider;

#if UNITY_EDITOR
	[SerializeField] private bool skipLoadData;
#endif

	[SerializeField]
	private string serverUrl;
	public string ServerUrl { get { return serverUrl; } }

	private int userID;
	public int UserID { get { return userID; } }
    private string userName;
    public string UserName {  get { return userName; } }

    private string userMail;
    public string UserMail { get { return userMail; } }

    private string userOcupation; //TODO: Change to enum
    public string UserOcupation { get { return userOcupation; } }

	private Texture2D userProfilePicture;
	public Texture2D UserProfilePicture { get { return userProfilePicture; } }

	private static string trakerUrl = "/tracker/save/";

	private static UserData instance = null;

	private List<Alarm> userCreatedAlarms;
	private string profilePictureURL;
	private string userKey;

	public static UserData Instance
	{
		get
		{
			return instance;
		}
	}

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}

		instance = this;
		DontDestroyOnLoad(this.gameObject);
	}

	private void Start()
	{
		LoadUserData();

		userCreatedAlarms = new List<Alarm>();
		speechToText.Initialize();
	}

	private void LoadUserData()
	{
#if UNITY_EDITOR
		if (skipLoadData)
		{
			introController.DisableLoading();

			return;
		}
#endif
		if (!ES3.KeyExists("userID") || !ES3.KeyExists("userKey"))
		{
			Debug.Log("No data to load");
			introController.DisableLoading();

			return;
		}

		userID = ES3.Load<int>("userID");
		userName = ES3.Load<string>("userName");
		userMail = ES3.Load<string>("userMail");

		profilePictureURL = ES3.Load<string>("profilePictureURL");

		Debug.Log("Data Loaded!");

		CheckProfilePicture();
		InitializeControllers();
		introController.SkipIntro();
	}
	
	private void SaveUserData()
	{
		ES3.Save("userID", userID);
		ES3.Save("userName", userName);
		ES3.Save("userMail", userMail);
		ES3.Save("userKey", userKey);
		ES3.Save("profilePictureURL", profilePictureURL);

		Debug.Log("Data Saved!");
	}

	public void SavePodometerState(bool isActive)
	{
		ES3.Save("podometerActive", isActive);
	}

	public void SetUserData(int userID, string userName, string userMail, string profilePictureURL, string userKey)
	{
		this.userID = userID;
		this.userName = userName;
		this.userMail = userMail;
		this.profilePictureURL = profilePictureURL;
		this.userKey = userKey;
		InitializeControllers();
	}

	private void InitializeControllers()
	{
		UpdateProfilePicture(profilePictureURL);
		programController.UpdateProgramData();
		profileController.UpdateData();
		LoadAlarms();

		CheckPodometerState();

		SaveUserData();
	}

	private void CheckPodometerState()
	{
		bool podometerActive;
		if (ES3.KeyExists("podometerActive"))
		{
			podometerActive = ES3.Load<bool>("podometerActive");
		}
		else
		{
			podometerActive = true;
		}

		if (podometerActive)
		{
			podometer.StartPodometerWithTime();
			podometerSlider.value = 1.0f;
		}
		else
		{
			podometerSlider.value = 0.0f;
		}
	}

	//Checks if the current profile picture is the same in the server
	private void CheckProfilePicture()
	{
		var url = serverUrl + "/users/" + userID+ "/image";
		StartCoroutine(RequestServerGet(url, ProfilePictureHander, RequestErrorHandler));
	}

	//If the profile picture on server is diferent, download the new picture
	private void ProfilePictureHander(string text)
	{
		var answer = JsonConvert.DeserializeObject<ProfilePictureURL>(text);

		if(profilePictureURL != answer.profile_pic)
		{
			profilePictureURL = answer.profile_pic;
			StartCoroutine(DownloadPicture(answer.profile_pic));
		}
	}

	//If the profile picture on server is diferent, download the new picture
	public void SetProfilePicture(Texture2D newProfilePicture, string url)
	{
		profilePictureURL = url;
		userProfilePicture = newProfilePicture;

		ES3.Save("profilePictureURL", profilePictureURL);
	}

	public void UpdateProfilePicture(string url)
	{
		//Start "loading" animation
		profilePictureURL = url;
		StartCoroutine(DownloadPicture(url));
	}

	IEnumerator DownloadPicture(string url)
	{
		url = url.Trim();
		using (UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(url, false))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
				//TODO: Trigger Cant connect error
			}
			else if (webRequest.isHttpError)
			{
				//TODO: Error handling
			}
			else
			{
				userProfilePicture = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
				foregroundDataManager.LoadProfilePicture();
				profileController.LoadProfilePicture();
				profilePictureURL = url;
				ES3.Save("profilePictureURL", profilePictureURL);
				//Stop loading animation
			}
		}
	}

	public void UploadPodometerData(string steps)
	{
		StartCoroutine(UploadData(steps));
	}
	
	IEnumerator UploadData(string steps)
	{
		var podometerForm = new WWWForm();
		podometerForm.AddField("user_id", UserID);
		podometerForm.AddField("pasos_data", steps.ToString());

		using (UnityWebRequest webRequest = UnityWebRequest.Post(serverUrl + trakerUrl, podometerForm))
		{
			yield return webRequest.SendWebRequest();
		}
	}

	public void AddAlarm(Alarm alarm)
	{
		var url = serverUrl + "/alarmas/save/";

		var form = new WWWForm();
		form.AddField("user_id", userID);
		form.AddField("title", alarm.alarmTitle);
		form.AddField("notificationId", alarm.notificationId);
		form.AddField("alarmTime", alarm.alarmTime.ToString(new CultureInfo("es-ES")));

		StartCoroutine(RequestServerPost(url, form, PostAlarmHander, RequestErrorHandler));
	}

	public void RemoveAlarm(int id)
	{
		var url = serverUrl + "/alarmas/delete/";

		var form = new WWWForm();
		form.AddField("user_id", userID);
		form.AddField("notificationId", id);

		StartCoroutine(RequestServerPost(url, form, DeleteAlarmHandler, RequestErrorHandler));
	}

	public void DeleteAlarmHandler(string text)
	{
		Debug.Log(text);
	}

	public List<Alarm> GetAllAlarms()
	{
		return userCreatedAlarms;
	}

	private void LoadAlarms()
	{
		string url = serverUrl + "/alarmas/get/" + userID;
		StartCoroutine(RequestServerGet(url, RequestAlarmsHandler, RequestErrorHandler));
	}

	private void RequestAlarmsHandler(string text)
	{
		Debug.Log(text);
		var answer = JsonConvert.DeserializeObject<AlarmRequest>(text);
		var alarms = answer.resultado;

		foreach(var alarm in alarms)
		{
			organizerController.CreateSavedAlarm(alarm.nombre, alarm.notificacion_hora, alarm.notificacion_id);
		}
	}

	private void PostAlarmHander(string text)
	{
		Debug.Log(text);
	}

	public void DeleteAccountData()
	{
		foreach(var alarm in userCreatedAlarms)
		{
			NotificationsController.Instance.CancelNotification(alarm.notificationId);
		}

		userCreatedAlarms = new List<Alarm>();
		userID = 0;
		userName = "";
		userMail = "";

		DeleteUserData();
		introController.ReturnToIntro();
		foregroundDataManager.HideForeground();
	}

	private void DeleteUserData()
	{
		ES3.DeleteKey("userID", "Neurohit.es3");
		ES3.DeleteKey("userName", "Neurohit.es3");
		ES3.DeleteKey("userMail", "Neurohit.es3");
		ES3.DeleteKey("userKey", "Neurohit.es3");
		ES3.DeleteKey("podometerActive", "Neurohit.es3");
	}

	private void RequestErrorHandler(string text)
	{
		Debug.Log("Error " + text);
	}

	IEnumerator RequestServerGet(string url, Action<string> onComplete, Action<string> onError)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				onError.Invoke(webRequest.error);
			}
			else if (webRequest.isHttpError)
			{
				onError.Invoke(webRequest.downloadHandler.text);
			}
			else
			{
				onComplete.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	IEnumerator RequestServerPost(string url, WWWForm form, Action<string> onComplete, Action<string> onError)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, form))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
			}
			else if (webRequest.isHttpError)
			{
				Debug.Log(webRequest.error);
			}
			else
			{
				onComplete.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	private class AlarmRequest
	{
		private bool ok;
		public List<UserAlarmsRequest> resultado;
	}
	
	private class UserAlarmsRequest
	{
		public int id;
		public string nombre;
		public string notificacion_hora;
		public int notificacion_id;
		public int usuario_id;
	}

	private class ProfilePictureURL
	{
		public bool ok;
		public string profile_pic;
	}
}
