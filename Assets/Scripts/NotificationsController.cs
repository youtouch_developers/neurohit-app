﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

#if UNITY_ANDROID
using Unity.Notifications.Android;
#endif

#if UNITY_IOS
using Unity.Notifications.iOS;
#endif

public class NotificationsController : MonoBehaviour
{
	[SerializeField] private string notificationChannelId;
	[SerializeField] private string notificationChannelName;

	private List<AndroidNotification> currentNotifications;
	private AndroidNotificationChannel channel;

	private static NotificationsController instance = null;

	private string devideToken;

	public static NotificationsController Instance
	{
		get
		{
			return instance;
		}
	}
	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Destroy(this.gameObject);
		}

		instance = this;
		DontDestroyOnLoad(this.gameObject);
		InitializeDeviceNotifications();
	}

	private void InitializeDeviceNotifications()
	{
#if UNITY_ANDROID
		SetNotificationChannel();
#endif

#if UNITY_IOS
		StartCoroutine(RequestAuthorization());
#endif

	}

#if UNITY_ANDROID
	private void SetNotificationChannel()
	{
		channel = new AndroidNotificationChannel()
		{
			Id = notificationChannelId,
			Name = notificationChannelName,
			Importance = Importance.Default,
			Description = "NeuroHit Notifications"
		};

		AndroidNotificationCenter.RegisterNotificationChannel(channel);

		currentNotifications = new List<AndroidNotification>();
	}
#endif

#if UNITY_IOS
	IEnumerator RequestAuthorization()
	{
		var authOption = AuthorizationOption.Alert | AuthorizationOption.Badge;
		using(var req = new AuthorizationRequest(authOption, true))
		{
			while (!req.IsFinished)
			{
				yield return null;
			};

			devideToken = req.DeviceToken;
		}
	}
#endif

#if UNITY_ANDROID
	public int CreateNotification(string title, string text, DateTime time)
	{
		var newNotification = new AndroidNotification();
		newNotification.Title = "Recordatorio";
		newNotification.Text = text;
		newNotification.FireTime = time;

		currentNotifications.Add(newNotification);

		return AndroidNotificationCenter.SendNotification(newNotification, channel.Id);
	}
#endif

#if UNITY_IOS
	private int CreateIOSNotification(string title, string text, DateTime time)
	{
		var timeTrigger = new iOSNotificationTimeIntervalTrigger()
		{
			TimeInterval = time - DateTime.Now,
			Repeats = false
		};

		var id = UnityEngine.Random.Range(1, 10000000);

		var notification = new iOSNotification()
		{
			Identifier = id.ToString(),
			Title = title,
			Body = text,
			ShowInForeground = true,
			ForegroundPresentationOption = (PresentationOption.Alert | PresentationOption.Sound),
			CategoryIdentifier = "category_a",
			ThreadIdentifier = "thread1",
			Trigger = timeTrigger
		};

		iOSNotificationCenter.ScheduleNotification(notification);

		return id;
}
#endif

	public void CancelNotification(int notificationID)
	{
#if UNITY_ANDROID
		CancelAndroidNotification(notificationID);
#endif

#if UNITY_IOS
		CancelIOSNotification(notificationID);
#endif
	}

#if UNITY_ANDROID
	public void CancelAndroidNotification(int notificationID)
	{
		AndroidNotificationCenter.CancelNotification(notificationID);
	}
#endif

	public bool CheckNotificationStatus(int notificationID)
	{
#if UNITY_ANDROID
		return CheckAndroidNotificationStatus(notificationID);
#endif
#if UNITY_IOS
		return false;
#endif
	}

#if UNITY_IOS
	private void CancelIOSNotification(int notificationID)
	{
		iOSNotificationCenter.RemoveDeliveredNotification(notificationID.ToString());
	}
#endif

#if UNITY_ANDROID
	public bool CheckAndroidNotificationStatus(int notificationID)
	{
#if UNITY_EDITOR
		return false;
#endif
		var status = AndroidNotificationCenter.CheckScheduledNotificationStatus(notificationID);

		if (status == NotificationStatus.Scheduled || status == NotificationStatus.Delivered)
			return true;

		return false;
	}
#endif
}
