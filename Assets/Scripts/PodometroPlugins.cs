﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

public class PodometroPlugins : MonoBehaviour 
{ 
    AndroidJavaClass unityClass;
    AndroidJavaObject unityActivity;
    AndroidJavaClass customClass;
    public TMP_Text PodometerText;
    public TMP_Text PodometerTextProfile;
    public float new_Steps;
    public float CheckRate;
    public float MaxCheckRate;
    public float old_steps;
    public bool serviceStarted;
    public DataManager manager;

    [SerializeField] private float startUpTime;

    private bool updateNextStep;

    private void Start()
    {
        manager.Load();

        if (manager.data.CurrentDate != System.DateTime.Now.ToString("yyyy/MM/dd").ToString())
        {
            old_steps = manager.data.CountedSteps;
        }
        sendActivityReference("com.watafo.unitylib.ServiceStarter");
    }

    public void StartPodometerWithTime()
	{
        StartCoroutine(WaitToStart());
	}

    IEnumerator WaitToStart()
	{
        yield return new WaitForSeconds(startUpTime);

        startService();
	}


    void sendActivityReference(string packageName)
    {
        unityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        unityActivity = unityClass.GetStatic<AndroidJavaObject>("currentActivity");
        customClass = new AndroidJavaClass(packageName);
        customClass.CallStatic("receiveActivityInstance", unityActivity);
    }

    public void startService(bool saveValue = false)
    {
        if (!serviceStarted) 
        {
            customClass.CallStatic("StartCustomService");
            serviceStarted = true;
            new_Steps = customClass.CallStatic<float>("GetStep");
            if (old_steps != 0)
            {
                new_Steps = new_Steps - old_steps;
                if (new_Steps < 0) 
                {
                    new_Steps *= -1;
                }
            }
            //PodometerText.text = new_Steps.ToString();
            //PodometerTextProfile.text = new_Steps.ToString();
            manager.data.CountedSteps = new_Steps;
            manager.data.CurrentDate = System.DateTime.Now.ToString("yyyy/MM/dd").ToString();
            manager.Save();
        }

		if (saveValue)
		{
            UserData.Instance.SavePodometerState(true);
		}
    }

    private void Update()
    {
        //reactiva el servicio para actualizar la informacion un promedio de 15 segundos entre cada llamada 
        if (CheckRate >= MaxCheckRate && serviceStarted) 
        {
            StopService();
            startService();
            CheckRate = 0;

			if (updateNextStep)
            {
                updateNextStep = false;
                UserData.Instance.UploadPodometerData(manager.data.CountedSteps.ToString());
            }
            //Steps = customClass.CallStatic<float>("GetStep");
            //PodometerText.text = ""+Steps.ToString();
        }
        CheckRate += Time.deltaTime;

    }

    public void StopService(bool saveValue = false)
    {
        if (serviceStarted) 
        {
            customClass.CallStatic("StopCustomService");
            manager.data.CountedSteps = new_Steps;
            manager.data.CurrentDate = System.DateTime.Now.ToString("yyyy/MM/dd").ToString();
            manager.Save();
            serviceStarted = false;
        }

        if (saveValue)
        {
            UserData.Instance.SavePodometerState(false);
        }
    }

	private void OnApplicationFocus(bool focus)
	{
        if (!focus)
            return;

        updateNextStep = true;
    }
}
