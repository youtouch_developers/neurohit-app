﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.Android;
using System;

public class LoginController : MonoBehaviour
{
	[SerializeField] private Nav_Manager navigationManager;
	[SerializeField] private Nav_Panel loginNavigation;
	[SerializeField] private ForegroundDataManager foregroundController;

	[Header("Login")]
	[SerializeField] private TMP_InputField loginEmail;
	[SerializeField] private TMP_InputField loginPassword;
	[SerializeField] private TMP_Text loginErrorTextField;
	[SerializeField] private GameObject loadingImage;

	[Header("Register")]
	[SerializeField] private TMP_InputField registerEmail;
	[SerializeField] private TMP_InputField registerName;
	[SerializeField] private TMP_InputField registerOcupation;
	[SerializeField] private TMP_InputField registerPassword1;
	[SerializeField] private TMP_InputField registerPassword2;
	[SerializeField] private TMP_Text registerErrorTextField;
	[SerializeField] private TMP_Text passwordErrorTextField;
	[SerializeField] private int passwordMinimumCharacters;

	[Header("Register2")]
	[SerializeField] private GameObject factorsRoot;

	[Header("Password Recovery")]
	[SerializeField] private TMP_InputField emailRecovery;
	[SerializeField] private TMP_Text emailRecoveryError;
	[SerializeField] private TMP_Text emailRecoveryText;

	[Header("Errores")]
	[SerializeField] private string networkErrorText;
	[SerializeField] private string loginErrorText;
	[SerializeField] private string emailUsedErrorText;
	[SerializeField] private string emailNotValidErrorText;
	[SerializeField] private string fillAllFiedlsErrorText;
	[SerializeField] private string passwordsCharactersErrorText;
	[SerializeField] private string passwordsErrorText;
	[SerializeField] private GameObject factorsErrorLayer;
	[SerializeField] private GameObject registerErrorLayer;

	readonly string loginURL = "/users/login";
	readonly string emailCheckURL = "/users/register";
	readonly string registerURL = "/users/register/finish";
	readonly string factorsURL = "/users/register/factores";

	private bool isWaitingForServer;

	//Conected to Log In Buttton
	public void StartLogin()
	{
		if (isWaitingForServer)
			return;

		loginErrorTextField.text = "";
		if (loginPassword.text.Length == 0 || !IsValidEmail(loginEmail.text))
		{
			loginErrorTextField.text = loginErrorText;

			return;
		}

		isWaitingForServer = true;
		loadingImage.SetActive(true);

		var userForm = new WWWForm();
		userForm.AddField("email", loginEmail.text);
		userForm.AddField("password", loginPassword.text);

		var url = UserData.Instance.ServerUrl + loginURL;


		StartCoroutine(PostFormServerRequest(url, userForm, LoginCompleteHandler, LoginHttpErrorHandler, LoginNetworkErrorHandler, true));
	}

	private bool IsValidEmail(string email)
	{
		try
		{
			var addr = new System.Net.Mail.MailAddress(email);

			var indexOfDot = email.IndexOf('.');

			return addr.Address == email && indexOfDot > 0 && (indexOfDot > email.IndexOf('@') && indexOfDot < (email.Length - 1));
		}
		catch
		{
			return false;
		}
	}

	private void LoginNetworkErrorHandler(string error)
	{
		loginErrorTextField.text = "Error de conexión";
		Debug.Log(error);
	}

	private void LoginHttpErrorHandler(string error)
	{
		loginErrorTextField.text = "Error de conexión";
		Debug.Log(error);
	}

	private void LoginCompleteHandler(string text)
	{
		Debug.Log(text);
		var serverResponse = JsonConvert.DeserializeObject<ServerUserResponse>(text);

		if (!serverResponse.ok)
		{
			loginErrorTextField.text = serverResponse.resultado.mensaje;

			return;
		}
		var loggedUser = serverResponse.usuario;
		UserData.Instance.SetUserData(loggedUser.id, loggedUser.nombre, loggedUser.email, serverResponse.profile_pic, loggedUser.accessToken);

		MoveToHomeScreen();
	}

	public void ValidateRegisterData()
	{
		if (isWaitingForServer)
			return;

		registerErrorTextField.text = "";
		passwordErrorTextField.text = "";

		if (registerEmail.text.Length == 0 || registerName.text.Length == 0 ||
			registerPassword1.text.Length == 0)
		{
			registerErrorTextField.text = fillAllFiedlsErrorText;

			return;
		}
		else if(registerPassword1.text.Length < passwordMinimumCharacters)
		{			
			passwordErrorTextField.text = passwordsCharactersErrorText;

			return;
		}
		else if (registerPassword1.text != registerPassword2.text)
		{
			passwordErrorTextField.text = passwordsErrorText;

			return;
		}
		else if (!IsValidEmail(registerEmail.text))
		{
			registerErrorTextField.text = emailNotValidErrorText;

			return;
		}

		isWaitingForServer = true;
		loadingImage.SetActive(true);

		var emailForm = new WWWForm();
		emailForm.AddField("email", registerEmail.text);

		var url = UserData.Instance.ServerUrl + emailCheckURL;
		StartCoroutine(PostFormServerRequest(url, emailForm, EmailAvailableHandler, EmailErrorHandler, EmailNetworkErrorHandler, true));
	}

	private void EmailErrorHandler(string error)
	{
		registerErrorTextField.text = emailUsedErrorText;
	}

	private void EmailNetworkErrorHandler(string error)
	{
		registerErrorTextField.text = networkErrorText;
	}

	private void EmailAvailableHandler(string text)
	{
		loginNavigation.MoveWindow(2);
	}

	public void SendRegisterData()
	{
		var newUser = new WWWForm();
		newUser.AddField("email", registerEmail.text);
		newUser.AddField("nombre", registerName.text);
		newUser.AddField("password", registerPassword1.text);
		loadingImage.SetActive(true);

		var url = UserData.Instance.ServerUrl + registerURL;
		StartCoroutine(PostFormServerRequest(url, newUser, ResegisterCompleteHandler, RegisterErrorHandler, RegisterErrorHandler, false));
	}

	private void ResegisterCompleteHandler(string text)
	{
		var serverResponse = JsonConvert.DeserializeObject<ServerUserResponse>(text);
		var loggedUser = serverResponse.usuario;
		UserData.Instance.SetUserData(loggedUser.id, loggedUser.nombre, loggedUser.email, serverResponse.profile_pic, loggedUser.accessToken);

		Debug.Log("User Created:" + loggedUser.nombre);

		GetFactorsOrder();
	}

	private void RegisterErrorHandler(string error)
	{
		registerErrorLayer.SetActive(true);
		Debug.Log(error);
	}

	public void GetFactorsOrder()
	{
		isWaitingForServer = true;

		var dragables = factorsRoot.GetComponentsInChildren<DraggableUI>();
		var factors = new List<UserStressFactor>();
		foreach (var dragable in dragables)
		{
			var newFactor = new UserStressFactor();
			newFactor.id = int.Parse(dragable.transform.GetChild(0).name);
			newFactor.position = int.Parse(dragable.transform.parent.name);
			factors.Add(newFactor);
		}

		var factorsJson = JsonConvert.SerializeObject(factors);

		var factorsForm = new WWWForm();
		factorsForm.AddField("id", UserData.Instance.UserID);
		factorsForm.AddField("factores", factorsJson);

		var url = UserData.Instance.ServerUrl + factorsURL;

		StartCoroutine(PostFormServerRequest(url, factorsForm, RegistrationCOmpletedHandler, FactorsDataErrorHandler, FactorsDataErrorHandler, true));
	}

	private void RegistrationCOmpletedHandler(string text)
	{
		MoveToIntroductionScreen();
	}

	private void FactorsDataErrorHandler(string error)
	{
		factorsErrorLayer.SetActive(true);
		Debug.Log(error);
	}

	public void RequestPasswordRecovery()
	{
		var email = emailRecovery.text;
		emailRecoveryError.text = "";
		emailRecoveryText.text = "";

		if (IsValidEmail(email))
		{
			if (isWaitingForServer)
				return;

			isWaitingForServer = true;
			var url = UserData.Instance.ServerUrl + "/users/recover";
			var form = new WWWForm();
			form.AddField("email", email);

			StartCoroutine(PostFormServerRequest(url, form, RequestPasswordHandler, RequestPasswordErrorHandler, RequestPasswordNetworkErrorHandler, true));
		}
		else
		{
			emailRecoveryError.text = "Email no válido";
		}
	}

	private void RequestPasswordHandler(string text)
	{
		Debug.Log(text);
		emailRecoveryText.text = "Correo de recuperación enviado";
	}

	private void RequestPasswordErrorHandler(string text)
	{
		Debug.Log(text);
		emailRecoveryText.text = "Error";
	}

	private void RequestPasswordNetworkErrorHandler(string text)
	{
		Debug.Log(text);
		emailRecoveryText.text = "Error";
	}

	private IEnumerator PostFormServerRequest(string url, WWWForm form, Action<string> onResponse, Action<string> onHttpError, Action<string> onNetworkError, bool flagServerRequest)
	{
		using(UnityWebRequest webRequest = UnityWebRequest.Post(url, form))
		{
			yield return webRequest.SendWebRequest();

			if (flagServerRequest)
			{
				isWaitingForServer = false;
				loadingImage.SetActive(false);
			}

			if (webRequest.isNetworkError)
			{
				onNetworkError.Invoke(webRequest.error);

			}
			else if (webRequest.isHttpError)
			{
				onHttpError.Invoke(webRequest.downloadHandler.text);
			}
			else
			{
				onResponse.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	public void MoveWindow(int index)
	{
		if (isWaitingForServer)
			return;

		loginNavigation.MoveWindow(index);
	}

	public void MoveToHomeScreen()
	{
		navigationManager.MoveCanvas(2);
		foregroundController.ShowEverythingForeground();
		navigationManager.ResetButtons();
	}

	private void MoveToIntroductionScreen()
	{
		navigationManager.MoveCanvas(7);
	}

	private class LogInUser
	{
		public int id;
		public string nombre;
		public string apellido;
		public string email;
		public string accessToken;
		public int estado;
	}

	private class LoginResultado
	{
		public string mensaje;
	}

	private class ServerUserResponse
	{
		public bool ok;
		public LogInUser usuario;
		public string profile_pic;
		public string accessToken;
		public string mensaje;
		public LoginResultado resultado;
	}

	private class UserStressFactor
	{
		public int id;
		public int position;
	}
}
