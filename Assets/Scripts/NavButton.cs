﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.ProceduralImage;
using UnityEngine.UI;
using DG.Tweening;

public class NavButton : MonoBehaviour
{
	[SerializeField] private ProceduralImage selectedOverlap;
	[SerializeField] private ProceduralImage selectedIcon;
	[SerializeField] private ProceduralImage deselectedIcon;
	[SerializeField] private Button button;
	[SerializeField] private float animationDuration;
	[SerializeField] private bool startOn;

	public void Start()
	{
		if (startOn)
		{
			Select();
		}
		else
		{
			Deselect();
		}
	}

	public void Select()
	{
		selectedOverlap.DOFillAmount(1.0f, animationDuration);
		selectedIcon.enabled = true;
		deselectedIcon.enabled = false;
		DisableButton();
	}

	public void Deselect()
	{
		selectedOverlap.fillAmount = 0.0f;
		selectedIcon.enabled = false;
		deselectedIcon.enabled = true;
		EnableButton();
	}

	public void DisableButton()
	{
		button.enabled = false;
	}

	public void EnableButton()
	{
		button.enabled = true;
	}
}
