﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class ButtonSlider : MonoBehaviour
{
    [SerializeField] private Color onColor;
    [SerializeField] private Color offColor;
    [SerializeField] private Slider slider;
	[SerializeField] private Image image;
	[SerializeField] private UnityEvent onActivate;
	[SerializeField] private UnityEvent onDeactivate;

	private bool isActive;

	private void Start()
	{
		slider.onValueChanged.AddListener(OnValueChange);
		OnValueChange(slider.value);
		isActive = slider.value == 1.0f ? true : false;
	}

	public void OnValueChange(float value)
	{
		if(value == 0.0f)
		{
			image.color = offColor;
		}
		else
		{
			image.color = onColor;
		}
    }

	public void ChangeValue()
	{
		if (isActive)
		{
			isActive = false;
			slider.value = 0.0f;

			if (onDeactivate != null)
				onDeactivate.Invoke();
		}
		else
		{
			isActive = true;
			slider.value = 1.0f;

			if(onActivate != null)
				onActivate.Invoke();
		}
	}
}
