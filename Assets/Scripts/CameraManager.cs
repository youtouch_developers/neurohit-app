﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraManager : MonoBehaviour
{
	[SerializeField] private int maxPictureSize;
	[SerializeField] private Text testText;

	//Manages Permission and Takes Pictures.
   public string TakePicture(Action<Texture2D> onPictureTaken, Action onError)
	{
		if (NativeCamera.IsCameraBusy())
			return "";

		var picturePath = "";
		NativeCamera.Permission permission = NativeCamera.TakePicture((path) =>
		{
			if (path != null)
			{
				// Create a Texture2D from the captured image
				Texture2D texture = NativeCamera.LoadImageAtPath(path, maxPictureSize);
				if (texture == null)
				{
					Debug.Log("Couldn't load texture from " + path);
					return;
				}
				onPictureTaken.Invoke(texture);
				picturePath = path;
			}
		}, maxPictureSize,true, NativeCamera.PreferredCamera.Front);

		return picturePath;
	}
}
