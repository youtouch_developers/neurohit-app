﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DraggableUI : EventTrigger
{
    public string current;
    private bool dragging;
    [SerializeField] private Transform oldParent, newParent;
    private Transform lastParent;

    private void Awake()
    {
        oldParent = this.transform.parent;
        lastParent = this.transform.parent.parent;
        current = this.transform.parent.name;
    }

    public void Update()
    {
        if (dragging)
        {
            this.transform.position = new Vector2(this.transform.position.x, Input.mousePosition.y);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "List_object")
        {
            if (collision != this.transform.parent)
            {
                if (dragging)
                {
                    newParent = collision.transform;
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "List_object")
        {
            if (collision != this.transform.parent)
            {
                if (dragging)
                {
                    newParent = null;
                }
            }
        }
    }

    public override void OnPointerDown(PointerEventData eventData)
    {
        dragging = true;
        oldParent = transform.parent;
        this.transform.parent = lastParent;
    }

    public override void OnPointerUp(PointerEventData eventData)
    {
        dragging = false;
        if (newParent != null) // Replace inputfields
        {
            if (newParent.Find("InputField"))
            {
                Transform oldField = newParent.Find("InputField");
                oldField.parent = oldParent;
                SetRect(oldField);

                this.transform.parent = newParent;
                SetRect(transform);
            }
            else
            {
                transform.parent = newParent;
                SetRect(transform);
            }
        }
        else
        {
            this.transform.parent = oldParent;
            SetRect(transform);
        }
        current = transform.parent.name;
    }
    
    public void SetRect(Transform obj) // Reset anchored points & pivot
    {
        obj.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.5f, 0.5f);
        obj.GetComponent<RectTransform>().anchorMax = new Vector2(1f, 1f);
        obj.GetComponent<RectTransform>().anchorMin = new Vector2(0f, 0f);
        obj.GetComponent<RectTransform>().offsetMax = new Vector2(0, 0);
        obj.GetComponent<RectTransform>().offsetMin = new Vector2(135, 0);
    }
}
