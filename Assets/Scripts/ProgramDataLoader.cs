﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class ProgramDataLoader : MonoBehaviour
{
	[SerializeField] private ProgramController programController;

	#region Programs

	private static string programURL = "https://neurohit.azurewebsites.net/api/programa/user/";
	private static string programEnrollURL = "https://neurohit.azurewebsites.net/api/programa/enroll/";
	private static string programCapsuleURL = "https://neurohit.azurewebsites.net/api/programa/capsula/";
	private static string dailyQuizURL = "https://neurohit.azurewebsites.net/api/programa/quiz/diario";

	public void UpdateProgramData()
	{
		StartCoroutine(GetProgramData());
	}

	private IEnumerator GetProgramData()
	{
		UnityWebRequest webRequest = UnityWebRequest.Get(programURL + UserData.Instance.UserID);

		yield return webRequest.SendWebRequest();

		if (webRequest.isNetworkError)
		{
			Debug.Log(webRequest.error);
			//TODO: Trigger Cant connect error
		}
		else if (webRequest.isHttpError)
		{
			Debug.Log(webRequest.error);
			Debug.Log(webRequest.downloadHandler.text);
			//TODO: Trigger Email or Password not correct

			StartCoroutine(EnollToProgram());
		}
		else
		{
			Debug.Log(webRequest.downloadHandler.text);

			var serverResponse = JsonConvert.DeserializeObject<UserProgram>(webRequest.downloadHandler.text);

			if(serverResponse.resultado.preguntas_diarias.Count > 0)
			{
				Debug.Log("Answered");
				SetDailyQuestionsData(serverResponse.resultado.preguntas_diarias);
			}
			else
			{
				Debug.Log("Not Answered");
				programController.SetDailyQuestions(false, DailyQuizAnswerHandler);
			}

			StartCoroutine(GetCapsuleData(serverResponse.resultado.programas_capsulas_id));
		}
	}

	private void SetDailyQuestionsData(List<DailyQuiz> daily)
	{
		var answers = new List<int>();

		daily.ForEach(question => answers.Add(question.respuesta));
		//programController.SetDailyQuestionAnswers(answers);
	}

	IEnumerator EnollToProgram()
	{
		UnityWebRequest webRequest = UnityWebRequest.Get(programEnrollURL + UserData.Instance.UserID);

		yield return webRequest.SendWebRequest();

		if (webRequest.isNetworkError)
		{
			Debug.Log(webRequest.error);
			//TODO: Trigger Cant connect error
		}
		else if (webRequest.isHttpError)
		{
			Debug.Log(webRequest.error);
			Debug.Log(webRequest.downloadHandler.text);
			//TODO: Trigger Email or Password not correct
		}
		else
		{
			Debug.Log(webRequest.downloadHandler.text);

			var serverResponse = JsonConvert.DeserializeObject<UserEnrollData>(webRequest.downloadHandler.text);
			StartCoroutine(GetCapsuleData(serverResponse.programProgreso.programas_capsulas_id));
		}

	}

	IEnumerator GetCapsuleData(int capsuleID)
	{
		UnityWebRequest webRequest = UnityWebRequest.Get(programCapsuleURL + capsuleID);

		yield return webRequest.SendWebRequest();

		if (webRequest.isNetworkError)
		{
			Debug.Log(webRequest.error);
			//TODO: Trigger Cant connect error
		}
		else if (webRequest.isHttpError)
		{
			Debug.Log(webRequest.error);
			Debug.Log(webRequest.downloadHandler.text);
			//TODO: Trigger Email or Password not correct
		}
		else
		{
			Debug.Log(webRequest.downloadHandler.text);

			var serverResponse = JsonConvert.DeserializeObject<UserCapsuleDataResponse>(webRequest.downloadHandler.text);
			Debug.Log(serverResponse.resultado.capsula.titulo);
			Debug.Log(serverResponse.resultado.capsula.descripcion);

			//currentProgram = serverResponse.resultado;
		}
	}

	private void DailyQuizAnswerHandler(List<int> answers)
	{
		var dailyQuiz = new List<DailyQuiz>();

		for(var index = 0; index < answers.Count; index++)
		{
			var quiz = new DailyQuiz();
			quiz.id_pregunta = index;
			quiz.respuesta = answers[index];

			dailyQuiz.Add(quiz);
		}

		StartCoroutine(UploadDailyQuiz(dailyQuiz));
	}

	IEnumerator UploadDailyQuiz(List<DailyQuiz> answers)
	{
		var dailyQuizRequest = new DailyQuizRequest();
		dailyQuizRequest.user_id = UserData.Instance.UserID;
		dailyQuizRequest.respuestas = answers;

		var userForm = JsonConvert.SerializeObject(dailyQuizRequest, Formatting.Indented);

		UnityWebRequest webRequest = UnityWebRequest.Post(dailyQuizURL, userForm);
		webRequest.SetRequestHeader("Content-Type", "application/json");

		yield return webRequest.SendWebRequest();

		if (webRequest.isNetworkError)
		{
			Debug.Log(webRequest.error);
			//TODO: Trigger Cant connect error
		}
		else if (webRequest.isHttpError)
		{
			Debug.Log(webRequest.error);
			Debug.Log(webRequest.downloadHandler.text);
			//TODO: Trigger Email or Password not correct
		}
		else
		{
			Debug.Log(webRequest.downloadHandler.text);

			//currentProgram = serverResponse.resultado;
		}
	}

	private struct UserProgram
	{
		public bool ok;
		public CurrentProgramData resultado;
	}

	private class CurrentProgramData
	{
		public int id;
		public int user_id;
		public int promgramas_id;
		public int programas_capsulas_id;
		public string fecha_comenzado;
		public string feha_finalizado;
		public int active;
		public List<DailyQuiz> preguntas_diarias;
	}

	private class DailyQuiz
	{
		public int id_pregunta;
		public int respuesta;
	}

	private class DailyQuizRequest
	{
		public int user_id;
		public List<DailyQuiz> respuestas;
	}

	private class UserEnrollData
	{
		public bool ok;
		public string mensaje;
		public ProgrammProgress programProgreso;
	}

	private class ProgrammProgress
	{
		public int id;
		public int user_id;
		public int programas_id;
		public int programas_capsulas_id;
		public int active;
	}

	private class UserCapsuleDataResponse
	{
		public bool ok;
		public UserCapsuleDataResult resultado;
	}

	public class UserCapsuleDataResult
	{
		public CapsuleData capsula;
		public List<CapsuleVideos> videos;
	}

	public class CapsuleData
	{
		public int id;
		public int id_programas_categorias;
		public int id_programas_niveles;
		public string titulo;
		public string descripcion;
		public string id_playlists;
		public CategoryData programas_categoria;

	}

	public class CategoryData
	{
		public int id;
		public int id_programa;
		public string nombre_programas_categorias;
		public string descripcion_programas_categorias;
	}

	public class CapsuleVideos
	{
		public int id;
		public string titulo;
		public string descripcion;
		public string url;
	}
	#endregion
}
