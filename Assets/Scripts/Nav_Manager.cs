﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.ProceduralImage;
using DG.Tweening;
using UnityEngine.UI;

public class Nav_Manager : MonoBehaviour
{
    private int index;
    [SerializeField] private Transform[] canvases;
    [SerializeField] private Transform current;
    [SerializeField] private NavButton[] navBtns;

    [Header("Foreground")]
    [SerializeField] private GameObject foreground;
    [SerializeField] private GameObject profilePic;
    [SerializeField] private GameObject foregroundInfo;

    private Sequence currentTween;

    private void Awake()
    {
        current = canvases[0];

        foreground.SetActive(false);
    }

    public void MoveCanvas(int i)
    {
        if (current != canvases[i])
        {
            if (currentTween != null)
                currentTween.Kill();

            for (int ind = 0; ind < canvases.Length; ind++)
            {
                if (canvases[ind] != canvases[i])
                {
                    canvases[ind].transform.localPosition = canvases[ind].parent.Find("LOWER").localPosition;
                    canvases[ind].gameObject.SetActive(false);
                }
                else
                {
                    currentTween = DOTween.Sequence();
                    currentTween.OnStart(() => 
                    {
                        canvases[i].gameObject.SetActive(true);
                        canvases[i].localPosition = canvases[i].parent.Find("LOWER").localPosition;
                        foreach (var button in navBtns)
                        {
                            button.DisableButton();
                        }
                    });
                    currentTween.AppendInterval(0.01f);
                    currentTween.Append(current.DOLocalMove(current.parent.Find("UPPER").localPosition, 0.75f));
                    currentTween.Join(canvases[i].DOLocalMove(canvases[i].parent.Find("Center").localPosition, 0.75f));
                    currentTween.OnComplete(() => {
                        current.gameObject.SetActive(false);
                        current = canvases[i];
                        foreach (var button in navBtns)
                        {
                            button.EnableButton();
                        }
                    });
                }
            }
        }
    }

    public void NavBtnControl(GameObject current)
    {
        for (int i = 0; i < navBtns.Length; i++)
        {
            if (navBtns[i].gameObject == current)
            {
                navBtns[i].Select();
            }
            else
            {
                navBtns[i].Deselect();
            }
        }
    }

    public void ProfilePictureSetActive(bool enable)
	{
        profilePic.SetActive(enable);
    }

    public void ForegroundInfoSetActive(bool enable)
	{
        foregroundInfo.SetActive(enable);
	}

    public void ForegroundSetActive(bool enable)
	{
        foreground.SetActive(enable);
	}

    public void ResetButtons()
	{
        NavBtnControl(navBtns[0].gameObject);
	}
}
