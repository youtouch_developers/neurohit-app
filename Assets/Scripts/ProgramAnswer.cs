﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public abstract class ProgramAnswer : MonoBehaviour
{
	[SerializeField] private RawImage icon;
	protected int questionID;
	public int QuestionID { get { return questionID; } }

	protected int currentAnswer;
	public int CurrentAnswer { get { return currentAnswer; } }

	protected string title;
	public string Title { get { return title; } }

	public abstract void Initialize(int initialValue, string question, int id, string title);

	public void SetIcon(Texture2D texture)
	{
		icon.texture = texture;

		if (texture.width != 0.0f && texture.height != 0.0f)
		{
			var ratio = (float)texture.width / (float)texture.height;
			icon.GetComponent<AspectRatioFitter>().aspectRatio = ratio;
		}
	}
}
