﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.ProceduralImage;
using TMPro;

public class SelectOneButton : MonoBehaviour
{
    [SerializeField] private ProceduralImage buttonImage;
    [SerializeField] private TMP_Text buttonText;
    [SerializeField] private Sprite selectedSprite;
    [SerializeField] private Sprite deselectedSprite;
    [SerializeField] private Color selectedTextColor;
    [SerializeField] private Color deselectedTextColor;

    public int answerIndex;
    public Action<int> onOptionSelected;

    public void Initialize(int index, string text, Action<int> selectedCallback, bool startSelected)
	{
        answerIndex = index;
        buttonText.text = text;

        buttonImage.sprite = startSelected? selectedSprite : deselectedSprite;
		buttonText.color = startSelected ? selectedTextColor : deselectedTextColor;

        onOptionSelected = selectedCallback;
    }

    public void OnSelected()
    {
        buttonImage.sprite = selectedSprite;
        buttonText.color = selectedTextColor;

        onOptionSelected.Invoke(answerIndex);
    }

    public void Deselect(int index)
	{
        if (answerIndex == index)
            return;

        buttonImage.sprite = deselectedSprite;
        buttonText.color = deselectedTextColor;
    }
}
