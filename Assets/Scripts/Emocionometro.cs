﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.UI.ProceduralImage;
using DG.Tweening;

public class Emocionometro : MonoBehaviour
{
	[SerializeField] private Nav_Panel navEmocionometro;
	[SerializeField] private CameraManager phoneCamera;
	[SerializeField] private RawImage image;

	[SerializeField] private List<ProceduralImage> levelBars;
	[SerializeField] private List<TMP_Text> percentages;
	[SerializeField] private float animationFillTime;
	[SerializeField] private GameObject loadingLayer;
	[SerializeField] private GameObject errorLayer;

	private static string emocionometroURL = "/emotion/check";

	private bool isWaitingForServer;

	//Resets previous values
	//Calls for the phone camera, if a picture is taken goes to OnPictureTakenHandler.
	//If not goes to ErrorHandler
	public void TakePicture()
	{
		if (isWaitingForServer)
			return;

		isWaitingForServer = true;

		levelBars.ForEach(bar => bar.fillAmount = 0.0f);
		percentages.ForEach(percentage => percentage.text = "0.0%");

		phoneCamera.TakePicture(OnPictureTakenHandler, CameraErrorHandler);
	}

	//On Picture taken, creates the texture and moves the window. 
	private void OnPictureTakenHandler(Texture2D picture)
	{
		StartCoroutine(WaitForPicture(picture));
	}

	IEnumerator WaitForPicture(Texture2D picture)
	{
		navEmocionometro.MoveWindow(1);

		yield return new WaitForSeconds(1.0f);

		image.texture = picture;
		if (picture.width != 0.0f && picture.height != 0.0f)
		{
			var ratio = (float)picture.width / (float)picture.height;
			image.GetComponent<AspectRatioFitter>().aspectRatio = ratio;
		}

		loadingLayer.SetActive(true);
		errorLayer.SetActive(false);

		CopyTextureData(picture);
	}

	private void CameraErrorHandler()
	{
		isWaitingForServer = false;
	}

	private IEnumerator UploadImage(Texture2D texture)
	{
		var textureBytes = ImageConversion.EncodeToPNG((texture));

		var imageUploadForm = new WWWForm();
		imageUploadForm.AddBinaryData("file", textureBytes);

		//var url = UserData.Instance.ServerUrl + emocionometroURL;
		var url = "https://neurohit.azurewebsites.net/api" + emocionometroURL;
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, imageUploadForm))
		{
			yield return webRequest.SendWebRequest();

			isWaitingForServer = false;
			loadingLayer.SetActive(false);

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
				errorLayer.SetActive(true);
			}
			else if (webRequest.isHttpError)
			{
				Debug.Log(webRequest.error);
				errorLayer.SetActive(true);
			}
			else
			{
				var serverResponse = JsonConvert.DeserializeObject<ServerResponse>(webRequest.downloadHandler.text);
				ReadServerResponse(serverResponse.resultado[0]);
			}
		}
	}

	//Set data to the corresponding graphic
	private void ReadServerResponse(Resultado photoAnalysis)
	{
		UpdateLevelBar(0, photoAnalysis.faceAttributes.emotion.fear);
		UpdateLevelBar(1, photoAnalysis.faceAttributes.emotion.sadness);
		UpdateLevelBar(2, photoAnalysis.faceAttributes.emotion.digust + photoAnalysis.faceAttributes.emotion.contempt/2.0f);
		UpdateLevelBar(3, photoAnalysis.faceAttributes.emotion.happiness);
		UpdateLevelBar(4, photoAnalysis.faceAttributes.emotion.surprise);
		UpdateLevelBar(5, photoAnalysis.faceAttributes.emotion.neutral);
		UpdateLevelBar(6, photoAnalysis.faceAttributes.emotion.anger + photoAnalysis.faceAttributes.emotion.contempt / 2.0f);
	}

	//Sets bars data and starts animations.
	private void UpdateLevelBar(int index, float value)
	{
		levelBars[index].DOFillAmount(value, animationFillTime);
		percentages[index].text = value.ToString("0.0%");
	}

	//Copies a texture data so there are no problemas with read/write permissions. 
	private void CopyTextureData(Texture2D texture)
	{
		var tempTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
		Graphics.Blit(texture, tempTexture);

		RenderTexture previous = RenderTexture.active;
		RenderTexture.active = tempTexture;
		var newTexture2d = new Texture2D(texture.width, texture.height);
		newTexture2d.ReadPixels(new Rect(0, 0, tempTexture.width, tempTexture.height), 0, 0);
		newTexture2d.Apply();

		RenderTexture.active = previous;
		RenderTexture.ReleaseTemporary(tempTexture);

		StartCoroutine(UploadImage(newTexture2d));
	}

	#region Server Response Classes
	private class ServerResponse
	{
		public bool ok;
		public string mensaje;
		public List<Resultado> resultado;
	}

	private class Resultado
	{
		public string faceId;
		public FaceRectangle faceRectangle;
		public FaceAttributes faceAttributes;
	}

	private class FaceRectangle
	{
		public int top;
		public int left;
		public int width;
		public int height;
	}

	private class FaceAttributes
	{
		public Emotions emotion;
	}

	private class Emotions
	{
		public float anger;
		public float contempt;
		public float digust;
		public float fear;
		public float happiness;
		public float neutral;
		public float sadness;
		public float surprise;
	}
	#endregion
}
