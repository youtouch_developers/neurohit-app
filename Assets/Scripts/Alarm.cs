﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class Alarm : MonoBehaviour
{
    [SerializeField] private TMP_Text timeTextbox;
    [SerializeField] private TMP_Text descriptionTextbox;

	private DeleteAlarmConfirmation confirmationWindow;

	public DateTime alarmTime;
	public int notificationId;
	public string alarmTitle;

	public bool Initialize(int hour, int minutes, string description, DeleteAlarmConfirmation confirmationWindow, bool createNotification = true)
	{
		if (!CheckIfCorrectTime(hour, minutes))
		{
			Debug.Log("Time Not Valid");
			Destroy(gameObject);

			return false;
		}

		this.confirmationWindow = confirmationWindow;
		timeTextbox.text = hour + ":" + (minutes < 10 ? "0" + minutes : minutes.ToString());
        descriptionTextbox.text = description;
		alarmTitle = description;

		var timeNow = DateTime.Now;
		var minutesLeft = hour * 60 + minutes - timeNow.Hour * 60 - timeNow.Minute;
		alarmTime = timeNow.AddMinutes(minutesLeft);
		alarmTime = alarmTime.AddSeconds(-alarmTime.Second);

		if(createNotification)
			CreateNotification(description);

		return true;
	}

	private bool CheckIfCorrectTime(int hour, int minutes)
	{
		var timeNow = DateTime.Now;

		if (timeNow.Hour > hour)
			return false;

		if (timeNow.Hour == hour && timeNow.Minute > minutes)
			return false;

		return true;
	}

    private void CreateNotification(string description)
	{
		notificationId = NotificationsController.Instance.CreateNotification(alarmTitle, description, alarmTime);
	}

    public void DestroyAlarm()
	{
		NotificationsController.Instance.CancelNotification(notificationId);
		UserData.Instance.RemoveAlarm(notificationId);
		Destroy(gameObject);
	}

    public void DestroyAlarmPrompt()
	{
		confirmationWindow.TriggerConfirmation(DestroyAlarm);
	}
}
