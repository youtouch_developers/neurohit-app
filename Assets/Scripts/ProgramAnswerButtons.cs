﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using TMPro;

public class ProgramAnswerButtons : ProgramAnswer
{
	[SerializeField] private TMP_Text questionField;
	[SerializeField] private SelectOneButton buttonPrefab;
	[SerializeField] private Transform buttonsParent;

	private List<SelectOneButton> buttons;
	private int questionId;

	[SerializeField] private string pregunta;

	public override void Initialize(int initialValue, string question, int id, string title)
	{
		currentAnswer = initialValue;
		questionField.text = question;
		questionId = id;
		this.title = title;
	}

	public void InstantiateQuestions(List<string> answers)
	{
		buttons = new List<SelectOneButton>();

		foreach (Transform child in buttonsParent)
		{
			Destroy(child.gameObject);
		}

		for(var index = 0; index < answers.Count; index++)
		{
			if(answers[index] != "")
				InstantiateButton(answers[index], index);
		}
	}

	private void InstantiateButton(string text, int index)
	{
		var newButton = Instantiate(buttonPrefab, buttonsParent);
		newButton.Initialize(index, text, OnButtonSelected, index == currentAnswer);
		buttons.Add(newButton);
	}

	public void OnButtonSelected(int selectedIndex)
	{
		currentAnswer = selectedIndex;

		buttons.ForEach(button => button.Deselect(currentAnswer));
	}
}
