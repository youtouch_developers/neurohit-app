﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
//this class will manage all the data of the user.
public class DataManager : MonoBehaviour
{

    public PodometroData data;                  
    public string file = "UserData.txt";        

    public void Save() 
    {

        string json = JsonUtility.ToJson(data);
        WriteToFile(file,json);
    
    }
    public void Load() 
    {
        data = new PodometroData();
        string json = ReadFromFile(file);
        JsonUtility.FromJsonOverwrite(json,data);
    
    }
    private void WriteToFile(string Filename , string Json) 
    {
        
        string path = Getfilepath(Filename);
        FileStream fileStream = new FileStream(path,FileMode.Create);
        using (StreamWriter writer = new StreamWriter(fileStream)) 
        {
            writer.Write(Json);
        }
    
    }

    private string ReadFromFile(string filename) 
    {

        string path = Getfilepath(filename);
        if (File.Exists(path))
        {

            using (StreamReader reader = new StreamReader(path)) 
            {

                string json = reader.ReadToEnd();
                return json;
            
            }
        
        }
        else
        {
            data.CurrentDate = System.DateTime.Now.ToString("yyyy/MM/dd").ToString();
            data.CountedSteps = 0;
        }
        return "";
    
    }
    private string Getfilepath(string filename) 
    {
        return Application.persistentDataPath + "/" + filename;
    
    }
}
