﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class DailyQuestionsController : MonoBehaviour
{
    [SerializeField] private List<string> questions;
    [SerializeField] private List<string> descriptions;
    [SerializeField] private List<string> questionsTitle;
    [SerializeField] private List<string> expectedAnswers;
    [SerializeField] private TMP_Text questionTextBox;
    [SerializeField] private TMP_Text descriptionTextBox;
    [SerializeField] private TMP_Text answerTextBox;
    [SerializeField] private SpeechToTextAnswer speechToText;
    [SerializeField] private string errorText;
    [SerializeField] private string answerText;
    [SerializeField] private HoldAndReleaseButton recordButton;
    [SerializeField] private List<ProgramAnswerAnimation> resultAnimations;
    [SerializeField] private Nav_Panel navigationPanel;

    [Header("Final Results")]
    [SerializeField] private Transform quizScoreParent;
    [SerializeField] private ProgramAnswerAnimation questionScorePrefab;

    [Header("Speech to Text Animation")]
    [SerializeField] private float minTalkingAnimationSize;
    [SerializeField] private float midTalkingAnimationSize;
    [SerializeField] private float maxTalkingAnimationSize;
    [SerializeField] private Transform talkingAnimationTransform;

    private int currentQuestionIndex;
    private int currentAnswer;
    private List<int> answers;

    private bool isRecording;
    private bool isCompleted;

    private Action<List<int>> onQuizComplete;

    public void Initialize(bool isAnswered, Action<List<int>> onQuizCompleteCallback = null)
	{
		if (isAnswered)
        {
            questionTextBox.text = "Cuestionario diario respondido";
            descriptionTextBox.text = "Vuelve mañana para continuar con tu programa";
            answerTextBox.text = "";
            isCompleted = true;

            return;
		}

        if(onQuizCompleteCallback != null)
            onQuizComplete = onQuizCompleteCallback;

        currentQuestionIndex = 0;
        currentAnswer = 0;
        answers = new List<int>();

        answerTextBox.text = "";

        recordButton.onClick += StartRecording;
        recordButton.onRelease += StopRecording;

        isRecording = false;
    }

    public void SetQuestions(List<string> questions, List<string> descriptions)
	{
        if (isCompleted)
            return;

        this.questions = questions;
        this.descriptions = descriptions;
        questionTextBox.text = questions[currentQuestionIndex];
        descriptionTextBox.text = descriptions[currentQuestionIndex];
    }

    public void SetAnswers(List<int> answers)
	{
        this.answers = answers;
        isCompleted = true;
    }

    public void NextQuestion()
	{
		if (isCompleted)
        {
            PlayResultsAnimation();

            return;
        }

        if (currentAnswer == 0)
        {
            ErrorHandler("Reponde la pregunta");

            return;
        }

        answers.Add(currentAnswer);

        currentQuestionIndex++;

        if (currentQuestionIndex >= questions.Count)
        {
            isCompleted = true;
            PlayResultsAnimation();

            if(onQuizComplete != null)
                onQuizComplete.Invoke(answers);

            return;
		}

        currentAnswer = 0;
        questionTextBox.text = questions[currentQuestionIndex];
        descriptionTextBox.text = descriptions[currentQuestionIndex];

        answerTextBox.text = "";
    }

    public void StartRecording()
	{
        if (isRecording)
            return;

        isRecording = true;
        answerTextBox.text = "";
        speechToText.StartRecording(SelectAnswerHandler, ErrorHandler, OnRecording, OnStoppedRecording, OnRecordingError);
    }

    private void OnRecording(float maxVolume, float currentVolume)
    {
        if (currentVolume > 1.0f)
        {
            var volume = Mathf.Clamp(currentVolume / maxVolume, 0.0f, 1.0f);
            var scale = midTalkingAnimationSize + (maxTalkingAnimationSize - midTalkingAnimationSize) * volume;
            talkingAnimationTransform.localScale = new Vector3(scale, scale, 1.0f);
        }
        else
        {
            var volume = Mathf.Clamp(currentVolume / maxVolume, 0.0f, 1.0f);
            var scale = minTalkingAnimationSize + (midTalkingAnimationSize - minTalkingAnimationSize) * volume;
            talkingAnimationTransform.localScale = new Vector3(scale, scale, 1.0f);
        }
    }

    private void OnStoppedRecording()
    {
        talkingAnimationTransform.localScale = new Vector3(minTalkingAnimationSize, minTalkingAnimationSize, 1.0f);
    }

    private void OnRecordingError(string error)
	{
        answerTextBox.text = error;
    }

    public void StopRecording()
	{
        if (!isRecording)
            return;

        isRecording = false;
        speechToText.StopRecordButtonOnClickHandler();
	}

    private void PlayResultsAnimation()
    {
        navigationPanel.MoveWindow(1);
        ShowFinalScores();

        questionTextBox.text = "Cuestionario diario respondido";
        descriptionTextBox.text = "Vuelve mañana para continuar con tu programa";
    }

    public void ShowFinalScores()
    {
        foreach (Transform child in quizScoreParent)
        {
            Destroy(child.gameObject);
        }

        for (var index = 0; index < answers.Count; index++)
        {
            InstantiateFinalScore(answers[index], questionsTitle[index]);
        }
    }

    private void InstantiateFinalScore(int score, string title)
    {
        var newQuestion = Instantiate(questionScorePrefab, quizScoreParent);
        newQuestion.Initialize(score, title);
    }

    private void SelectAnswerHandler(List<string> detectedWords)
    {
        if (detectedWords.Count == 0)
        {
            ErrorHandler("No se detectó respuesta");
            return;
        }

        detectedWords.Reverse();

        foreach (var word in detectedWords)
        {
            var fixedWord = word.ToLower();
            if (expectedAnswers.Contains(fixedWord))
            {
                ParseAnswer(fixedWord);
                return;
            }
        }

        ErrorHandler("No se detectó respuesta");
    }

    private void ParseAnswer(string answer)
    {
        if (answer.Length > 1)
        {
            currentAnswer = ChangeToInteger(answer);
        }
        else
        {
            currentAnswer = int.Parse(answer);
        }

        if (currentAnswer != 0)
        {
            answerTextBox.text = answerText + currentAnswer.ToString();
        }
        else
        {
            answerTextBox.text = errorText;
        }
    }

    private void ErrorHandler(string error)
    {
        answerTextBox.text = errorText + error;
    }

    private int ChangeToInteger(string answer)
	{
        answer = answer.ToLower();

		switch (answer)
		{
            case "uno":
                    
                return 1;
            case "dos":

                return 2;
            case "tres":

                return 3;
            case "cuatro":

                return 4;
            case "cinco":

                return 5;
            case "seis":

                return 6;
            case "siete":

                return 7;
            case "ocho":

                return 8;
            case "nueve":

                return 9;
            case "diez":

                return 10;
            default:

                return 0;
        }
	}
}
