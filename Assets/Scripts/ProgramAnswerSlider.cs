﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using TMPro;
using UnityEngine.UI.ProceduralImage;

public class ProgramAnswerSlider : ProgramAnswer
{
	[Header("Question Elements")]
	[SerializeField] private TMP_Text questionTextBox;
	[SerializeField] private TMP_Text sliderButtonNumber;
	[SerializeField] private Image fillImage;
	[SerializeField] private Slider slider;
	[SerializeField] private int initialValue;

	public override void Initialize(int initialValue, string question, int id, string title)
	{
		questionID = id;
		questionTextBox.text = question;
		this.title = title;

		slider.value = initialValue;
		slider.onValueChanged.AddListener(SelectAnswer);

		SelectAnswer(initialValue);
	}

	public void SelectAnswer(float value)
	{
		currentAnswer = (int)value;
		sliderButtonNumber.text = currentAnswer.ToString();

		var fill = 1.0f - (value - 1.0f)/9.0f;
		fillImage.fillAmount = fill;
	}
}
