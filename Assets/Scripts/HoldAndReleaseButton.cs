﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class HoldAndReleaseButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	private bool pointerDown;

	public event Action onClick;
	public event Action onRelease;


	public void OnPointerDown(PointerEventData eventData)
	{
		pointerDown = true;

		if (onClick != null)
			onClick.Invoke();
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		pointerDown = false;

		if (onRelease != null)
			onRelease.Invoke();
	}
}
