﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ProgramAnswerAnimation : MonoBehaviour
{
	[SerializeField] private RectTransform answerRectTransform;
	[SerializeField] private RectTransform percentage;
	[SerializeField] private TMP_Text text;
	[SerializeField] private TMP_Text title;
	[SerializeField] private Image imageFill;
	[SerializeField] private float animationDuration;
	[SerializeField] private float maxDistance;

	private float fill;
	private float percentagePoisition;

	public void Initialize(int answer, string title)
	{
		StartAnimation(answer);
		this.title.text = title;
	}

	public void StartAnimation(int answer)
	{
		fill = answer / 10.0f;
		percentagePoisition = maxDistance * fill;

		text.text = fill.ToString("0%");

		imageFill.fillAmount = 1.0f;
		imageFill.DOFillAmount((1.0f-fill), animationDuration);
		percentage.anchoredPosition = new Vector2(0.0f, percentage.anchoredPosition.y);
		percentage.DOAnchorPos(new Vector2(percentagePoisition, percentage.anchoredPosition.y), animationDuration);
	}
}
