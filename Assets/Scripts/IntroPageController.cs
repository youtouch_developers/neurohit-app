﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPageController : MonoBehaviour
{
	[SerializeField] private GameObject loadingLayer;
	[SerializeField] private Nav_Manager navigationManager;
	[SerializeField] private TouchNavPanel introPanel;
	[SerializeField] private int loadTime;

	public void SkipIntro()
	{
		StartCoroutine(WaitForSeconds());
	}

	IEnumerator WaitForSeconds()
	{
		yield return new WaitForSeconds(loadTime);

		DisableLoading();
		navigationManager.MoveCanvas(2);
		navigationManager.ForegroundSetActive(true);
		navigationManager.ProfilePictureSetActive(true);
		navigationManager.ForegroundInfoSetActive(true);
	}

	public void DisableLoading()
	{
		loadingLayer.SetActive(false);
	}

	public void ReturnToIntro()
	{
		navigationManager.MoveCanvas(0);
		introPanel.MoveWindow(0);
	}
}
