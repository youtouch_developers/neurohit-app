﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI.ProceduralImage;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Linq;
using System;
using System.Text;
using UnityEngine.UI;

public class ProgramController : MonoBehaviour
{
	[SerializeField] private TMP_Text title;
	[SerializeField] private TMP_Text titleQuestions;
	[SerializeField] private TMP_Text titleFinalResults;
	[SerializeField] private TMP_Text titleRecomendations;
	[SerializeField] private TMP_Text descriptionResults;
	[SerializeField] private TMP_Text description;
    [SerializeField] private TMP_Text videoTitle;
    [SerializeField] private TMP_Text videoDescription;
	[SerializeField] private TMP_Text authorVideo;
	[SerializeField] private TMP_Text authorProfesion;
	[SerializeField] private RawImage videoImage;
	[SerializeField] private ProgramVideo video;
	[SerializeField] private Nav_Panel panelProgram;
	[SerializeField] private TMP_Text titleCapsuleRecomendations;

	[Header("Quiz")]
	[SerializeField] private GameObject quizWindow;
	[SerializeField] private GameObject noQuizWindow;
	[SerializeField] private GameObject quizAnsweredWindow;
	[SerializeField] private GameObject errorWindow;
	[SerializeField] private Transform quizParent;
	[SerializeField] private Transform quizScoreParent;
	[SerializeField] private ProgramAnswerSlider sliderQuestionPrefab;
	[SerializeField] private ProgramAnswerButtons buttonQuestionPrefab;
	[SerializeField] private ProgramAnswerAnimation questionScorePrefab;
	[SerializeField] private int quizInitialValue;
	[SerializeField] private int buttonQuizInitialValue;

	[Header("Daily Quiz")]
	[SerializeField] private DailyQuestionsController dailyQuestions;

	[Header("Recomendation")]
	[SerializeField] private TMP_Text recomendationTitle;
	[SerializeField] private TMP_Text recomendationDescription;
	[SerializeField] private RawImage recomendationImage;

	[Header("Error")]
	[SerializeField] private GameObject errorLayer;

	[Header("Podometro")]
	[SerializeField] private TMP_Text podometroText;
	[SerializeField] private TMP_Text podometroProfileText;

	private UserCapsuleDataResult currentProgram;
	private List<ProgramAnswer> capsuleQuestions;
	private int quizId;
	private bool quizAnswered;
	private bool programActive;

	private static string programURL = "/programa/user/";
	private static string programEnrollURL = "/programa/enroll/";
	private static string programCapsuleURL = "/programa/capsula/";
	private static string dailyQuizURL = "/programa/quiz/diario";

	private void Start()
	{
		quizWindow.SetActive(false);
		noQuizWindow.SetActive(true);
	}

	public void SetData()
	{
		if (currentProgram.videos.Count == 0)
			return;

		var video = currentProgram.videos[0];

		videoTitle.text = video.titulo;

		videoDescription.text = video.descripcion;
		authorVideo.text = video.autor;
		authorProfesion.text = video.autor_profesion;

		descriptionResults.text = "Estos son los resultados diarios del programa " + video.titulo;

		StartCoroutine(DownloadPicture(video.video_thumb, OnVideoThumbnailDownloaded));
	}

	private void OnVideoThumbnailDownloaded(Texture2D texture)
	{
		videoImage.texture = texture;
	}

	public void UpdateProgramData()
	{
		var url = UserData.Instance.ServerUrl + programURL + UserData.Instance.UserID;
		StartCoroutine(GetServerRequest(url, ProgramDataResponseHandler, HttpErrorHandler, NetworkErrorHandler)); //400 bad request
	}

	private void HttpErrorHandler(string text)
	{
		Debug.Log(text);
		quizWindow.SetActive(false);
		quizAnsweredWindow.SetActive(false);
		noQuizWindow.SetActive(false);
		errorWindow.SetActive(true);
	}

	private void NetworkErrorHandler(string error)
	{
		Debug.Log(error);
		quizWindow.SetActive(false);
		quizAnsweredWindow.SetActive(false);
		noQuizWindow.SetActive(false);
		errorWindow.SetActive(true);
	}

	private void ProgramDataResponseHandler(string text)
	{
		var serverResponse = JsonConvert.DeserializeObject<UserProgram>(text);
		errorWindow.SetActive(false);
		Debug.Log(text);

		podometroText.text = serverResponse.resultado.pasos.ToString();
		podometroProfileText.text = serverResponse.resultado.pasos.ToString();

		RequestDailyQuestions();
		CheckDailyQuestionsStatus(serverResponse.resultado.preguntas_diarias);

		//If programas_id = 4, user has no current program.
		if (serverResponse.resultado.programas_id == 4)
		{
			quizWindow.SetActive(false);
			quizAnsweredWindow.SetActive(false);
			noQuizWindow.SetActive(true);
			programActive = false;

			return;
		}

		SetProgramInformation(serverResponse.resultado.programa, serverResponse.resultado.subtitulo);

		var url = UserData.Instance.ServerUrl + programCapsuleURL + serverResponse.resultado.programas_capsulas_id;
		StartCoroutine(GetServerRequest(url, CapsuleDataHandler, NetworkErrorHandler, NetworkErrorHandler));
	}

	//Set data on the program tab
	private void SetProgramInformation(UserProgramData data, string subtitulo)
	{
		title.text = data.nombre;
		titleQuestions.text = data.nombre;
		titleFinalResults.text = data.nombre;
		titleRecomendations.text = data.nombre;
		description.text = data.descripcion;
		titleCapsuleRecomendations.text = "Recomendaciones para " + subtitulo;

	}

	//Checks if the daily questions have been answered
	private void CheckDailyQuestionsStatus(List<DailyQuiz> dailyQuestions)
	{
		if (dailyQuestions.Count > 0)
		{
			SetDailyQuestionsData(dailyQuestions);
		}
		else
		{
			SetDailyQuestions(false, DailyQuizAnswerHandler);
		}
	}

	//If the daily questions have been answered, sets the results on the tab.
	private void SetDailyQuestionsData(List<DailyQuiz> daily)
	{
		var answers = new List<int>();

		daily.ForEach(question => answers.Add(question.respuesta));
		dailyQuestions.SetAnswers(answers);
		SetDailyQuestions(true, DailyQuizAnswerHandler);
	}

	private void RequestDailyQuestions()
	{
		var url = UserData.Instance.ServerUrl + "/programa/quiz/diario";

		StartCoroutine(GetServerRequest(url, DailyQuestionsHandler, DailyQuestionsErrorHandler, DailyQuestionsErrorHandler));
	}

	private void DailyQuestionsHandler(string text)
	{
		var serverResponse = JsonConvert.DeserializeObject<DailyQuizGetRequest>(text);

		var questions = new List<string>();
		var descriptions = new List<string>();
		foreach (var question in serverResponse.resultado)
		{
			questions.Add(question.titulo);
			descriptions.Add(question.descripcion);
		}

		dailyQuestions.SetQuestions(questions, descriptions);
	}

	private void DailyQuestionsErrorHandler(string text)
	{
		Debug.Log(text);
	}

	public void SetDailyQuestions(bool isAnswered, Action<List<int>> callback = null)
	{
		dailyQuestions.Initialize(isAnswered, callback);
	}

	//private void NewProgramHandler(string text)
	//{
	//	var serverResponse = JsonConvert.DeserializeObject<UserEnrollData>(text);
	//	var url = UserData.Instance.ServerUrl + programCapsuleURL + serverResponse.programProgreso.programas_capsulas_id;

	//	StartCoroutine(GetServerRequest(url, CapsuleDataHandler, NetworkErrorHandler, NetworkErrorHandler));
	//}

	//Serializes Capsule data and requests the program quiz data
	private void CapsuleDataHandler(string text)
	{
		var serverResponse = JsonConvert.DeserializeObject<UserCapsuleDataResponse>(text);

		currentProgram = serverResponse.resultado;
		programActive = false;
		SetData();

		var url = UserData.Instance.ServerUrl + programCapsuleURL + currentProgram.capsula.id + "/preguntas";
		var form = new WWWForm();
		form.AddField("user_id", UserData.Instance.UserID);

		StartCoroutine(PostServerRequest(url, form, CapsuleQuizDataHandler, NetworkErrorHandler, NetworkErrorHandler));
	}

	//If the quiz has been answered blocks the quiz.
	private void CapsuleQuizDataHandler(string text)
	{
		Debug.Log(text);
		var serverResponse = JsonConvert.DeserializeObject<CapsuleQuizResponse>(text);
		quizId = serverResponse.resultado.quiz[0].id_quiz;
		InstantiateQuiz(serverResponse.resultado.quiz[0].preguntas);

		if(serverResponse.resultado.quiz[0].respondido == 1)
		{
			quizWindow.SetActive(false);
			quizAnsweredWindow.SetActive(true);
			noQuizWindow.SetActive(false);
		}
		else
		{
			quizWindow.SetActive(true);
			noQuizWindow.SetActive(false);
			quizAnsweredWindow.SetActive(false);
			quizAnswered = false;
		}
	}

	//Instantiate the program quiz questions.
	private void InstantiateQuiz(List<QuizQuestions> questions)
	{
		foreach(Transform child in quizParent)
		{
			Destroy(child.gameObject);
		}

		capsuleQuestions = new List<ProgramAnswer>();

		questions.ForEach(question => SelectQuestionType(question));
	}

	private void SelectQuestionType(QuizQuestions question)
	{
		if(question.tipo_pregunta == "slider")
		{
			InstantiateSliderQuestion(question.descripcion_pregunta, question.id_pregunta, question.descripcion2_pregunta, question.url_img_pregunta);
		}
		else
		{
			InstantiateButtonsQuestion(question.descripcion_pregunta, question.id_pregunta, question.descripcion2_pregunta, question.url_img_pregunta, question.respuestas);
		}
	}

	private void InstantiateSliderQuestion(string question, int id, string title, string iconURL)
	{
		var newQuestion = Instantiate(sliderQuestionPrefab, quizParent);
		newQuestion.Initialize(quizInitialValue, question, id, title); //AGREGAR URL ICON
		StartCoroutine(RequestTexture(iconURL, newQuestion));

		capsuleQuestions.Add(newQuestion);
	}

	protected void InstantiateButtonsQuestion(string question, int id, string title, string iconURL, List<string> answers)
	{
		var newQuestion = Instantiate(buttonQuestionPrefab, quizParent);
		newQuestion.Initialize(buttonQuizInitialValue, question, id, title); //AGREGAR URL ICON
		newQuestion.InstantiateQuestions(answers);
		StartCoroutine(RequestTexture(iconURL, newQuestion));

		capsuleQuestions.Add(newQuestion);
	}

	protected IEnumerator RequestTexture(string url, ProgramAnswer question)
	{
		using (UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(url, false))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
				//TODO: Trigger Cant connect error
			}
			else if (webRequest.isHttpError)
			{
				Debug.Log(webRequest.downloadHandler.text);
				//TODO: Error handling
			}
			else
			{
				var texture = ((DownloadHandlerTexture)webRequest.downloadHandler).texture;
				question.SetIcon(texture);
			}
		}
	}

	public void ShowFinalScores()
	{
		errorLayer.SetActive(false);

		if (quizAnswered)
			return;

		foreach (Transform child in quizScoreParent)
		{
			Destroy(child.gameObject);
		}

		foreach(var question in capsuleQuestions)
		{
			InstantiateFinalScore(question.CurrentAnswer, question.Title);
		}

		StartUploadAnswers();
	}

	private void InstantiateFinalScore(int score, string title)
	{
		var newQuestion = Instantiate(questionScorePrefab, quizScoreParent);
		newQuestion.Initialize(score, title);
	}

	private void DailyQuizAnswerHandler(List<int> answers)
	{
		var dailyQuiz = new List<DailyQuiz>();

		for (var index = 0; index < answers.Count; index++)
		{
			var quiz = new DailyQuiz();
			quiz.id_pregunta = index;
			quiz.respuesta = answers[index];

			dailyQuiz.Add(quiz);
		}

		StartCoroutine(UploadDailyQuiz(dailyQuiz));
	}

	IEnumerator UploadDailyQuiz(List<DailyQuiz> answers)
	{
		var userForm = new WWWForm();
		userForm.AddField("user_id", UserData.Instance.UserID);

		foreach(var answer in answers)
		{
			userForm.AddField("respuesta" + answer.id_pregunta, answer.respuesta);
		}

		var url = UserData.Instance.ServerUrl + dailyQuizURL;
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, userForm))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
				//TODO: Trigger Cant connect error
			}
			else if (webRequest.isHttpError)
			{
				Debug.Log(webRequest.error);
				Debug.Log(webRequest.downloadHandler.text);
			}
			else
			{
				Debug.Log(webRequest.downloadHandler.text);
			}
		}
	}

	private IEnumerator GetServerRequest(string url, Action<string> onResponse, Action<string> onHttpError, Action<string> onNetworkError)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Get(url))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				onNetworkError.Invoke(webRequest.error);

			}
			else if (webRequest.isHttpError)
			{
				onHttpError.Invoke(webRequest.downloadHandler.text);
			}
			else
			{
				onResponse.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	private IEnumerator PostServerRequest(string url, WWWForm form, Action<string> onResponse, Action<string> onHttpError, Action<string> onNetworkError)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, form))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				onNetworkError.Invoke(webRequest.error);

			}
			else if (webRequest.isHttpError)
			{
				onHttpError.Invoke(webRequest.error);
			}
			else
			{
				onResponse.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	private void StartUploadAnswers()
	{
		StartCoroutine(PostRequestToServer(capsuleQuestions));
	}

	IEnumerator PostRequestToServer(List<ProgramAnswer> answers)
	{
		var url = UserData.Instance.ServerUrl + "/programa/capsula/quiz/save";
		var form = new WWWForm();
		form.AddField("user_id", UserData.Instance.UserID);
		form.AddField("quiz_id", quizId);

		for(var index = 0; index <answers.Count; index++)
		{
			form.AddField("respuesta"+(index+1), answers[index].CurrentAnswer);
		}

		Debug.Log("Uploading results");
		using (UnityWebRequest www = UnityWebRequest.Post(url, form))
		{
			yield return www.SendWebRequest();

			OnAnswerUploadedHandler(www);
		}
	}

	public void MoveToRecomendations()
	{
		if (!quizAnswered)
			return;

		panelProgram.MoveWindow(3);
	}

	//Downloads the video thumbnail
	IEnumerator DownloadPicture(string url, Action<Texture2D> onComplete)
	{
		using (UnityWebRequest webRequest = UnityWebRequestTexture.GetTexture(url, false))
		{
			yield return webRequest.SendWebRequest();

			if (webRequest.isNetworkError)
			{
				Debug.Log(webRequest.error);
				//TODO: Trigger Cant connect error
			}
			else if (webRequest.isHttpError)
			{
				//TODO: Error handling
			}
			else
			{
				onComplete(((DownloadHandlerTexture)webRequest.downloadHandler).texture);
				//Stop loading animation
			}
		}
	}

	private void OnAnswerUploadedHandler(UnityWebRequest www)
	{
		if (www.isNetworkError)
		{
			errorLayer.SetActive(true);

		}
		else if (www.isHttpError)
		{
			errorLayer.SetActive(true);
		}
		else
		{
			Debug.Log(www.downloadHandler.text);

			var serverResponse = JsonConvert.DeserializeObject<QuizSaveAnswer>(www.downloadHandler.text);
			recomendationTitle.text = serverResponse.recomendacion.titulo;
			recomendationDescription.text = serverResponse.recomendacion.descripcion;
			quizAnswered = true;
		}
	}

	#region Programs
	private class UserProgram
	{
		public bool ok;
		public UserProgramDataResponse resultado;
	}

	private class UserProgramDataResponse
	{
		public int id;
		public int user_id;
		public int programas_id;
		public int programas_capsulas_id;
		public string fecha_comenzado;
		public string fecha_finalizado;
		public UserProgramData programa;
		public UserProgramDataCapsule programas_capsula;
		public List<DailyQuiz> preguntas_diarias;
		public int active;
		public string subtitulo;
		public int pasos;
	}

	private class UserProgramData
	{
		public int id;
		public string nombre;
		public string descripcion;
	}

	private class UserProgramDataCapsule
	{
		public string titulo;
		public string descripcion;
	}

	private class DailyQuiz
	{
		public int id_pregunta;
		public int respuesta;
	}

	private class DailyQuizRequest
	{
		public int user_id;
		public List<DailyQuiz> respuestas;
	}

	private class UserEnrollData
	{
		public bool ok;
		public string mensaje;
		public ProgrammProgress programProgreso;
	}

	private class ProgrammProgress
	{
		public int id;
		public int user_id;
		public int programas_id;
		public int programas_capsulas_id;
		public int active;
	}

	private class UserCapsuleDataResponse
	{
		public bool ok;
		public UserCapsuleDataResult resultado;
	}

	private class UserCapsuleDataResult
	{
		public CapsuleData capsula;
		public List<CapsuleVideos> videos;
	}

	private class CapsuleData
	{
		public int id;
		public int id_programas_categorias;
		public int id_programas_niveles;
		public string titulo;
		public string descripcion;
		public string id_playlists;
		public CategoryData programas_categoria;

	}

	private class CategoryData
	{
		public int id;
		public int id_programa;
		public string nombre_programas_categorias;
		public string descripcion_programas_categorias;
	}

	private class CapsuleVideos
	{
		public int id;
		public string titulo;
		public string descripcion;
		public string url;
		public string autor;
		public string autor_profesion;
		public string video_thumb;
	}

	private class CapsuleQuizResponse
	{
		public bool ok;
		public QuizData resultado;
	}

	private class QuizData
	{
		public string titulo_quiz;
		public string descripcion_quiz;
		public List<Quiz> quiz;
	}

	private class Quiz
	{
		public int id_quiz;
		public List<QuizQuestions> preguntas;
		public int respondido;
	}

	private class QuizQuestions
	{
		public int id_pregunta;
		public string descripcion_pregunta;
		public string descripcion2_pregunta;
		public string tipo_pregunta;
		public string url_img_pregunta;
		public List<string> respuestas;
	}

	private class QuizSaveAnswer
	{
		public bool ok;
		public string mensaje;
		public Recomendation recomendacion;
	}

	private class Recomendation
	{
		public int id_capsula;
		public int nivel;
		public string titulo;
		public string descripcion;
	}

	private class DailyQuizGetRequest
	{
		public bool ok;
		public List<DailyQuizQuestion> resultado;
	}

	private class DailyQuizQuestion
	{
		public int id;
		public string titulo;
		public string descripcion;
		public int orden;
	}
	#endregion
}
