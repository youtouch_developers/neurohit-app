﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

#if UNITY_ANDROID
using UnityEngine.Android;
#endif

#if UNITY_IOS
using UnityEngine.iOS
#endif

public class AudioRecorder : MonoBehaviour
{
	[SerializeField] private AudioSource testAudioSource;
	[SerializeField] private int testMaxAudioLength;

	public void CallForAudioRecording()
	{
#if UNITY_ANDROID
		Debug.Log("Android Test Audio Recording");
		StartAudiorRecording(testAudioSource, testMaxAudioLength);
#elif UNITY_EDITOR
		Debug.Log("Test Audio Recording");
		StartRecording(testAudioSource, testMaxAudioLength);
#endif
	}

	private void checkforandroidpermission()
	{
		if (Permission.HasUserAuthorizedPermission(Permission.Microphone))
			return;

		Permission.RequestUserPermission(Permission.Microphone);
	}

	private void StartAudiorRecording(AudioSource audiosource, int maxaudiolength)
	{
		if (!Permission.HasUserAuthorizedPermission(Permission.Microphone))
		{
			checkforandroidpermission();

			return;
		}

		StartRecording(audiosource, maxaudiolength);
	}

	private void StartRecording(AudioSource audiosource, int maxaudiolength)
	{

		audiosource.clip = Microphone.Start(Microphone.devices[0].ToString(), true, maxaudiolength, AudioSettings.outputSampleRate);
		while (!(Microphone.GetPosition(null) > 0)) { }
	}

	#region Speech to Text

	public void StartSpeechToText()
	{

	}

	private void Awake()
	{
		//StartCoroutine(InitializeServices());
	}

	private IEnumerator RecordingHandler()
	{
		var audioRecording = Microphone.Start(Microphone.devices[0].ToString(), false, testMaxAudioLength, AudioSettings.outputSampleRate);
		yield return null;      // let m_RecordingRoutine get set..

		if (audioRecording == null)
		{
			//StopRecording();
			yield break;
		}

		yield return new WaitForSeconds(testMaxAudioLength);

	}

	#endregion
}
