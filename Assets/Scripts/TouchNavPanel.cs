﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;

public class TouchNavPanel : MonoBehaviour
{
    public Transform[] ventanas;
    [SerializeField] private Transform current;
    [SerializeField] private Vector3[] positions;
    [SerializeField] private List<GameObject> navigationButtons;
    [SerializeField] private UnityEvent onFinalPanel;
    [SerializeField] private UnityEvent onMove;

    [Header("Touch Options")]
    [SerializeField] private bool useTouch;
    [SerializeField] private float minTouchDistance;

    private int currentIndex;
    private bool isMoving;
    private Sequence currentTween;

    // Start is called before the first frame update
    void Awake()
    {
        current = ventanas[0];
        currentIndex = 0;
        positions = new Vector3[ventanas.Length];
        for (int i = 0; i < ventanas.Length; i++)
        {
            positions[i] = ventanas[i].localPosition;
        }
    }

    public void MoveWindow(int i)
    {
        navigationButtons.ForEach(button => button.SetActive(false));
        navigationButtons[i].SetActive(true);
        isMoving = true;

        if (currentTween != null)
            currentTween.Kill();

        Vector3 pos = new Vector3(
            -positions[i].x,
            transform.localPosition.y,
            transform.localPosition.z);
        currentTween = DOTween.Sequence();
        currentTween.OnStart(() => { ventanas[i].gameObject.SetActive(true); });
        currentTween.Append(transform.DOLocalMove(pos, 0.6f));
        currentTween.OnComplete(() => {
            //current.gameObject.SetActive(false);
            current = ventanas[i];
            currentIndex = i;
            isMoving = false;

            if(currentIndex == ventanas.Length - 1)
			{
                onFinalPanel.Invoke();
            }
			else
			{
                onMove.Invoke();
			}
        });
    }

    private void Update()
    {
        if (!useTouch)
            return;

        if (Input.touchCount == 0 || isMoving)
            return;

        var touch = Input.GetTouch(0);

        if (touch.phase == TouchPhase.Moved)
        {
            var distance = touch.deltaPosition.x;
            if ((distance > 0.0f && currentIndex == 0) || (distance < 0.0f && currentIndex == ventanas.Length - 1) || Mathf.Abs(distance) < minTouchDistance)
                return;

            if (distance > 0.0f)
            {
                MoveWindow(currentIndex - 1);
            }
            else
            {
                MoveWindow(currentIndex + 1);
            }
        }
    }
}
