﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI.ProceduralImage;
using UnityEngine.UI;

public class ForegroundDataManager : MonoBehaviour
{
	[SerializeField] private TMP_Text userName;
	[SerializeField] private RawImage profilePicture;

	[Header("Elements")]
	[SerializeField] private GameObject profilePictureObject;
	[SerializeField] private GameObject stepsTextObject;
	[SerializeField] private GameObject nameTextObject;

	private Sprite profilePictureSprite;

	private void Start()
	{
		LoadUserName();
		LoadProfilePicture();
	}

	private void LoadUserName()
	{
		userName.text = "Hola " + UserData.Instance.UserName;
	}

	public void LoadProfilePicture()
	{
		if (UserData.Instance.UserProfilePicture == null)
			return;

		profilePicture.texture = UserData.Instance.UserProfilePicture;
		var ratio = (float)UserData.Instance.UserProfilePicture.width / (float)UserData.Instance.UserProfilePicture.height;
		profilePicture.GetComponent<AspectRatioFitter>().aspectRatio = ratio;
	}

	public void ShowEverythingForeground()
	{
		gameObject.SetActive(true);
		profilePictureObject.SetActive(true);
		nameTextObject.SetActive(true);
		stepsTextObject.SetActive(true);
	}

	public void ProfileForeground()
	{
		gameObject.SetActive(true);
		profilePictureObject.SetActive(false);
		nameTextObject.SetActive(false);
		stepsTextObject.SetActive(false);
	}

	public void HideForeground()
	{
		gameObject.SetActive(false);
	}
}
