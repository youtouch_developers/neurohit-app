﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nav_Panel : MonoBehaviour
{
    public Transform[] ventanas;
    [SerializeField] private Transform current;
    [SerializeField] private Vector3[] positions;

    private Sequence currentTween;

    // Start is called before the first frame update
    void Awake()
    {
        current = ventanas[0];
        positions = new Vector3[ventanas.Length];
        for (int i = 0; i < ventanas.Length; i++)
        {
            positions[i] = ventanas[i].localPosition;
        }
    }
    
    public void MoveWindow(int i)
    {
        if (currentTween != null)
            currentTween.Kill();

        Vector3 pos = new Vector3(
            -positions[i].x,
            transform.localPosition.y,
            transform.localPosition.z);
        currentTween = DOTween.Sequence();
        currentTween.OnStart(() => { ventanas[i].gameObject.SetActive(true); });
        currentTween.Append(transform.DOLocalMove(pos, 0.6f));
        currentTween.OnComplete(() => {
            //current.gameObject.SetActive(false);
            current = ventanas[i];
        });
    }
}
