﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//this class will contain the user data  , countedsteps , date  , sensitivity etc.
[System.Serializable]
public class PodometroData
{
    public float CountedSteps;           //pasos contados
    public string CurrentDate;         //fecha actual
}
