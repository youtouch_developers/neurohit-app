﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using TMPro;
using Newtonsoft.Json;
using System;

public class ProfileController : MonoBehaviour
{
	[SerializeField] private RawImage profilePicture;
	[SerializeField] private int maxSize;
	[SerializeField] private ForegroundDataManager foregroundData;
	[SerializeField] private CameraManager phoneCamera;
	[SerializeField] private TMP_Text profileName;
	[SerializeField] private GameObject networkErrorLayer;
	[SerializeField] private GameObject cameraErrorLayer;
	[SerializeField] private GameObject closeSessionConfirmationLayer;

	[SerializeField] private TMP_Text passwordChangeField;

	private static string profilePictureURL = "https://neurohit.azurewebsites.net/api/users/register/image";

	private bool isWaitingForServer;

	public void UpdateData()
	{
		profileName.text = UserData.Instance.UserName;
	}

	public void UpdateFromCamera()
	{
		if (isWaitingForServer)
			return;

		isWaitingForServer = true;

		phoneCamera.TakePicture(CopyTextureData, CameraErrorHandler);
	}

	private void CameraErrorHandler()
	{
		cameraErrorLayer.SetActive(true);
	}

	public void UpdateFromGallery()
	{
		if (isWaitingForServer)
			return;

		isWaitingForServer = true;

		NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
		{
			if (path == null)
			{
				cameraErrorLayer.SetActive(true);
				isWaitingForServer = false;
				return;
			}

			var texture = NativeGallery.LoadImageAtPath(path);
			if (texture == null)
			{
				cameraErrorLayer.SetActive(true);
				isWaitingForServer = false;
				return;
			}

			CopyTextureData(texture);
		}, "Seleccionar imagen de perfil");
	}

	private void CopyTextureData(Texture2D texture)
	{
		var tempTexture = RenderTexture.GetTemporary(texture.width, texture.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
		Graphics.Blit(texture, tempTexture);

		RenderTexture previous = RenderTexture.active;
		RenderTexture.active = tempTexture;
		var newTexture2d = new Texture2D(texture.width, texture.height);
		newTexture2d.ReadPixels(new Rect(0, 0, tempTexture.width, tempTexture.height), 0, 0);
		newTexture2d.Apply();

		RenderTexture.active = previous;
		RenderTexture.ReleaseTemporary(tempTexture);

		StartCoroutine(UploadProfilePicture(newTexture2d));
	}

	private IEnumerator UploadProfilePicture(Texture2D texture)
	{
		var textureBytes = ImageConversion.EncodeToPNG((texture));

		var factorsForm = new WWWForm();
		factorsForm.AddBinaryData("file", textureBytes);
		factorsForm.AddField("user_id", UserData.Instance.UserID);

		using (UnityWebRequest webRequest = UnityWebRequest.Post(profilePictureURL, factorsForm))
		{
			//TODO: start spin animation
			yield return webRequest.SendWebRequest();

			isWaitingForServer = false;

			if (webRequest.isNetworkError)
			{
				networkErrorLayer.SetActive(true);
			}
			else if (webRequest.isHttpError)
			{
				networkErrorLayer.SetActive(true);
			}
			else
			{
				profilePicture.texture = texture;
				var ratio = (float)texture.width / (float)texture.height;
				profilePicture.GetComponent<AspectRatioFitter>().aspectRatio = ratio;

				var answer = JsonConvert.DeserializeObject<UploadPictureRequest>(webRequest.downloadHandler.text);
				UserData.Instance.SetProfilePicture((Texture2D)profilePicture.texture, answer.url);
				foregroundData.LoadProfilePicture();
				//TODO: stop spin animation
			}
		}
	}

	public void LoadProfilePicture()
	{
		profilePicture.texture = UserData.Instance.UserProfilePicture;
		var ratio = (float)UserData.Instance.UserProfilePicture.width / (float)UserData.Instance.UserProfilePicture.height;
		profilePicture.GetComponent<AspectRatioFitter>().aspectRatio = ratio;
	}

	public void CloseSession()
	{
		closeSessionConfirmationLayer.SetActive(false);
		UserData.Instance.DeleteAccountData();
	}

	public void RequestPasswordRecovery()
	{
		if (isWaitingForServer)
			return;

		isWaitingForServer = true;

		var url = UserData.Instance.ServerUrl + "/users/recover";
		var form = new WWWForm();
		form.AddField("email", UserData.Instance.UserMail);

		StartCoroutine(PostFormServerRequest(url, form, RequestPasswordHandler, RequestPasswordErrorHandler, RequestPasswordNetworkErrorHandler, true));
	}

	private void RequestPasswordHandler(string text)
	{
		Debug.Log(text);
		passwordChangeField.text = "Correo de cambio de contraseña enviado";
	}

	private void RequestPasswordErrorHandler(string text)
	{
		Debug.Log(text);
	}

	private void RequestPasswordNetworkErrorHandler(string text)
	{
		Debug.Log(text);
	}

	private IEnumerator PostFormServerRequest(string url, WWWForm form, Action<string> onResponse, Action<string> onHttpError, Action<string> onNetworkError, bool flagServerRequest)
	{
		using (UnityWebRequest webRequest = UnityWebRequest.Post(url, form))
		{
			yield return webRequest.SendWebRequest();

			if (flagServerRequest)
			{
				isWaitingForServer = false;
			}

			if (webRequest.isNetworkError)
			{
				onNetworkError.Invoke(webRequest.error);

			}
			else if (webRequest.isHttpError)
			{
				onHttpError.Invoke(webRequest.downloadHandler.text);
			}
			else
			{
				onResponse.Invoke(webRequest.downloadHandler.text);
			}
		}
	}

	private class UploadPictureRequest
	{
		public bool ok;
		public string mensaje;
		public string url;
	}
}
