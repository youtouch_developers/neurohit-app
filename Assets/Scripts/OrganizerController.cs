﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using System;
using System.Globalization;

public class OrganizerController : MonoBehaviour
{
    [SerializeField] private SpeechToTextAnswer speechToText;
    [SerializeField] private HoldAndReleaseButton recordButton;

    [SerializeField] private Alarm alarmPrefab;
    [SerializeField] private Transform alarmsPanel;
    [SerializeField] private DeleteAlarmConfirmation alarmConfirmation;

    [SerializeField] private TMP_InputField descriptionInput;
    [SerializeField] private TMP_InputField hoursInput;
    [SerializeField] private TMP_InputField minutesInput;
    [SerializeField] private GameObject overlayGameObject;
    [SerializeField] private GameObject overlayBackgroundGameObject;
    [SerializeField] private RectTransform overlayTransform;

    [SerializeField] private float overlayHidePosition;
    [SerializeField] private float overlayShowPosition;
    [SerializeField] private float overlayMovementTime;

    [Header("Speech to Text Animation")]
    [SerializeField] private float minTalkingAnimationSize;
    [SerializeField] private float midTalkingAnimationSize;
    [SerializeField] private float maxTalkingAnimationSize;
    [SerializeField] private Transform talkingAnimationTransform;

    [Header("Error")]
    [SerializeField] private GameObject errorLayer;
    [SerializeField] private TMP_Text errorField;


    private bool isRecording;

    private int hours;
    private int minutes;
    private string description;

    private static string hoursString = "horas";
    private static string hourString = "hora";
    private static string minutesString = "minutos";
    private static string minuteString = "minuto";

    private List<Alarm> createdAlarms;

    public void Start()
    {
        createdAlarms = new List<Alarm>();
        recordButton.onClick += StartRecording;
        recordButton.onRelease += StopRecording;
        errorLayer.SetActive(false);

        isRecording = false;
    }

    public void StartRecording()
    {
        if (isRecording)
            return;

        descriptionInput.text = "";
        hoursInput.text = "";
        minutesInput.text = "";
        errorField.text = "";
        isRecording = true;
        speechToText.StartRecording(AnswerRecognizedHandler, RecognizeErrorHandler, OnRecording, OnStoppedRecording, RecordingErrorHandler);
    }

    public void StopRecording()
    {
        if (!isRecording)
            return;

        isRecording = false;
        speechToText.StopRecordButtonOnClickHandler();
    }

    private void AnswerRecognizedHandler(List<string> answer)
    {
        string text = "";
        answer.ForEach(word => text += " " + word);

        SerializeText(answer);
    }

    private void OnRecording(float maxVolume, float currentVolume)
    {
        if (currentVolume > 1.0f)
        {
            var volume = Mathf.Clamp(currentVolume / maxVolume, 0.0f, 1.0f);
            var scale = midTalkingAnimationSize + (maxTalkingAnimationSize - midTalkingAnimationSize) * volume;
            talkingAnimationTransform.localScale = new Vector3(scale, scale, 1.0f);
        }
		else
        {
            var volume = Mathf.Clamp(currentVolume / maxVolume, 0.0f, 1.0f);
            var scale = minTalkingAnimationSize + (midTalkingAnimationSize - minTalkingAnimationSize) * volume;
            talkingAnimationTransform.localScale = new Vector3(scale, scale, 1.0f);
        }
    }

    private void OnStoppedRecording()
    {
        talkingAnimationTransform.localScale = new Vector3(minTalkingAnimationSize, minTalkingAnimationSize, 1.0f);
    }

    private void RecognizeErrorHandler(string error)
    {
        errorLayer.SetActive(true);
    }

    private void RecordingErrorHandler(string error)
	{
        errorField.text = error;
    }

    private void SerializeText(List<string> text)
	{
        text.ForEach(word => word = word.ToLower());
        var hoursIndex = text.IndexOf(hoursString);

        if(hoursIndex == -1)
        {
            hoursIndex = text.IndexOf(hourString);
        }

        if(hoursIndex == -1 || hoursIndex == 0)
		{
            descriptionInput.text = "Intentelo nuevamente";
            return;
		}

        if(text[hoursIndex - 1].Length > 2)
		{
            var parsedText = ChangeToInteger(text[hoursIndex - 1]);
            
            if(parsedText  == -1)
            {
                descriptionInput.text = "Intentelo nuevamente";

                return;
			}
			else
			{
                hours = parsedText;
            }
		}
		else
        {
            hours = int.Parse(text[hoursIndex - 1]);
        }

        var minutesIndex = text.IndexOf(minutesString);
        minutes = 0;

        if(minutesIndex == -1)
		{
            minutesIndex = text.IndexOf(minuteString);
        }

        if(minutesIndex > 0)
        {
            if (text[minutesIndex - 1].Length > 2)
            {
                var parsedText = ChangeToInteger(text[minutesIndex - 1]);

                if (parsedText == -1)
                {
                    minutes = 0;
                }
                else
                {
                    minutes = parsedText;
                }
            }
			else
			{
                minutes = int.Parse(text[minutesIndex - 1]);
            }
        }

        var descriptionIndex = minutesIndex > 0 ? minutesIndex: hoursIndex;
        descriptionIndex++;

        description = "";

        if (descriptionIndex < text.Count)
        {
            text.RemoveRange(0, descriptionIndex);
            text.ForEach(word => description += word + " ");
        }

        ShowText();
	}

    public void OpenNewAlarmOverlay()
	{
        overlayGameObject.SetActive(true);
        overlayBackgroundGameObject.SetActive(true);
        descriptionInput.text = "";

        overlayTransform.DOAnchorPosY(overlayShowPosition, overlayMovementTime);
    }

    public void CloseAlarmOverlay()
    {
        overlayBackgroundGameObject.SetActive(false);
        overlayTransform.DOAnchorPosY(overlayHidePosition, overlayMovementTime).OnComplete(()=>
        {
            overlayGameObject.SetActive(false);
        });

        descriptionInput.text = "";
        hoursInput.text = "";
        minutesInput.text = "";
    }

    private void ShowText()
    {
        descriptionInput.text = description;
        hoursInput.text = hours.ToString();
        minutesInput.text = minutes.ToString();
    }

    public void CreateNewAlarm()
	{
        InitializeAlarm();
    }

    private void InitializeAlarm()
    {
        var alarm = Instantiate(alarmPrefab, alarmsPanel);
        if(alarm.Initialize(hours, minutes, description, alarmConfirmation))
		{
            UserData.Instance.AddAlarm(alarm);
            createdAlarms.Add(alarm);
            hours = 0;
            minutes = 0;
            description = "";

            CloseAlarmOverlay();
        }
		else
		{
            errorField.text = "La hora ingresada debe ser mayor que la actual";
        }
    }

    public void CheckHours()
    {
        if (hoursInput.text == "")
            return;

        var newHours = int.Parse(hoursInput.text);
        if (newHours > 24)
        {
            hours = 24;
            hoursInput.text = "24";
        }
		else
		{
            hours = newHours;
		}
    }

    public void CheckMinutes()
    {
        if (minutesInput.text == "")
            return;

        var newMinutes = int.Parse(minutesInput.text);
        if(newMinutes > 60)
		{
            minutes = 60;
            minutesInput.text = "60";
		}
		else
		{
            minutes = newMinutes;
		}
	}

    public void CheckDescription()
	{
        description = descriptionInput.text;
	}

    public void CreateSavedAlarm(string text, string date, int id)
	{
        var alarmTime = DateTime.Parse(date, new CultureInfo("es-ES"));

        if (alarmTime < DateTime.Now)
            return;

        var alarm = Instantiate(alarmPrefab, alarmsPanel);

        var notificationCreated = NotificationsController.Instance.CheckNotificationStatus(id);
        if(alarm.Initialize(alarmTime.Hour, alarmTime.Minute, text, alarmConfirmation, !notificationCreated))
        {
            createdAlarms.Add(alarm);

            if (!notificationCreated)
            {
                UserData.Instance.RemoveAlarm(id);
                UserData.Instance.AddAlarm(alarm);
            }
		}
    }

    private int ChangeToInteger(string answer)
    {
        answer = answer.ToLower();

        switch (answer)
        {
            case "cero":

                return 0;
            case "uno":

                return 1;
            case "una":

                return 1;
            case "dos":

                return 2;
            case "tres":

                return 3;
            case "cuatro":

                return 4;
            case "cinco":

                return 5;
            case "seis":

                return 6;
            case "siete":

                return 7;
            case "ocho":

                return 8;
            case "nueve":

                return 9;
            case "diez":

                return 10;
            default:

                return -1;
        }
    }

    private void OnApplicationFocus(bool focus)
	{
        if (!focus || createdAlarms == null)
            return;

        foreach(var alarm in createdAlarms)
		{
            if(alarm != null)
			{
                if(alarm.alarmTime < DateTime.Now)
				{
                    alarm.DestroyAlarm();
				}
			}
		}
	}
}
