﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ProgramVideo : MonoBehaviour
{
    [SerializeField] private YoutubePlayer player;

    [SerializeField] public string url;
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private RenderTexture meshRenderer;
    [SerializeField] private GameObject videoPlayerObject;

    private void Awake()
    {
        meshRenderer.Release();
        videoPlayerObject.SetActive(false);
    }

    public void PlayVideo()
    {
        videoPlayerObject.SetActive(true);
        player.videoPlayer = videoPlayer;
        player.videoQuality = YoutubePlayer.YoutubeVideoQuality.STANDARD;
        player.Play(url);
    }

    public  void StopVideo()
	{
        player.Stop();
        videoPlayerObject.SetActive(false);
        meshRenderer.Release();
    }
}
