﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteAlarmConfirmation : MonoBehaviour
{
	[SerializeField] GameObject window;

	private Action onConfirmation;

	public void TriggerConfirmation(Action confirmationCallback)
	{
		window.SetActive(true);
		onConfirmation = confirmationCallback;
	}

	public void ConfirmDelete()
	{
		if (onConfirmation != null)
			onConfirmation.Invoke();

		window.SetActive(false);
	}

	public void Cancel()
	{
		window.SetActive(false);
		onConfirmation = null;
	}
}
