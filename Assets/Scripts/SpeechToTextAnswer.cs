﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using System;
using System.Linq;

public class SpeechToTextAnswer : MonoBehaviour
{
	[SerializeField] private List<string> contextPhrases;
	[SerializeField] private string noWordRecognizedError;

	private List<string> detectedWords;
	private GCSpeechRecognition speechRecognition;

	private Action<List<string>> onRecognize;
	private Action<string> onRecognizeError;
	private Action<float, float> onRecording;
	private Action onStopedRecording;
	private Action<string> onRecordError;

	private bool isInitialized;

	private void Awake()
	{
		isInitialized = false;
	}

	public void Initialize()
	{
		if (isInitialized)
			return; 

		isInitialized = true;

		speechRecognition = GCSpeechRecognition.Instance;
		speechRecognition.RecognizeSuccessEvent += RecognizeSuccessEventHandler;
		speechRecognition.RecognizeFailedEvent += RecognizeFailedEventHandler;

		speechRecognition.FinishedRecordEvent += FinishedRecordEventHandler;
		speechRecognition.StartedRecordEvent += StartedRecordEventHandler;
		speechRecognition.RecordFailedEvent += RecordFailedEventHandler;

#if UNITY_EDITOR
		speechRecognition.SetMicrophoneDevice(Microphone.devices[0]);
#endif

		if (!speechRecognition.HasMicrophonePermission())
		{
			speechRecognition.RequestMicrophonePermission(OnPermissionRequested);
			//speechRecognition.SetMicrophoneDevice(Microphone.devices[0]);
		}
		else
		{
			speechRecognition.SetMicrophoneDevice(Microphone.devices[0]);
		}
	}

	private void Update() //Only updates image, change somewhere?
	{
		if (!isInitialized)
			return;

		if (speechRecognition.IsRecording)
		{
			if (speechRecognition.GetMaxFrame() > 0)
			{

				float max = (float)speechRecognition.configs[speechRecognition.currentConfigIndex].voiceDetectionThreshold;
				float current = speechRecognition.GetLastFrame() / max;

				if (onRecording != null)
					onRecording.Invoke(max, current);
			}
		}
	}

	public void StartRecordButtonOnClickHandler()
	{
		Debug.Log("Start Recording");

		speechRecognition.StartRecord(false);
	}

	public void StopRecordButtonOnClickHandler()
	{
		Debug.Log("Stop Recording");

		speechRecognition.StopRecord();
	}

	private void StartedRecordEventHandler()
	{
		//Activate UI elements??????????

		Debug.Log("StartedRecordEventHandler");
	}

	private void RecordFailedEventHandler()
	{
		if (onRecordError != null)
			onRecordError.Invoke("Error al intentar grabar");
	}

	private void FinishedRecordEventHandler(AudioClip clip, float[] raw)
	{
		Debug.Log("Finished Recording");
		onStopedRecording.Invoke();

		if (clip == null)
			return;

		RecognitionConfig config = RecognitionConfig.GetDefault();
		config.languageCode = Enumerators.LanguageCode.es_CL.Parse();
		config.speechContexts = new SpeechContext[]
		{
				new SpeechContext()
				{
					phrases = contextPhrases.ToArray()
				}
		};
		config.audioChannelCount = clip.channels;

		GeneralRecognitionRequest recognitionRequest = new GeneralRecognitionRequest()
		{
			audio = new RecognitionAudioContent()
			{
				content = raw.ToBase64()
			},
			config = config
		};

		speechRecognition.Recognize(recognitionRequest);
	}

	private void RecognizeFailedEventHandler(string error)
	{
		Debug.Log(error);

		if(onRecognizeError != null)
			onRecognizeError.Invoke(error);
	}

	private void RecognizeSuccessEventHandler(RecognitionResponse recognitionResponse)
	{
		InsertRecognitionResponseInfo(recognitionResponse);
	}

	private void InsertRecognitionResponseInfo(RecognitionResponse recognitionResponse)
	{
		if (recognitionResponse == null || recognitionResponse.results.Length == 0)
		{
			if (onRecognizeError != null)
				onRecognizeError.Invoke(noWordRecognizedError);

			return;
		}

		var words = recognitionResponse.results[0].alternatives[0].words;

		if (words != null)
		{
			string times = string.Empty;

			detectedWords = new List<string>();
			foreach (var item in recognitionResponse.results[0].alternatives[0].words)
			{
				detectedWords.Add(item.word);
			}

			onRecognize.Invoke(detectedWords);
		}
	}

	public void StartRecording(Action<List<string>> callback, Action<string> errorCallback, Action<float,float> onRecording, Action onStopedRecording, Action<string> onErrorCallback)
	{
		if (!speechRecognition.HasMicrophonePermission())
		{
			speechRecognition.RequestMicrophonePermission(OnPermissionRequested);

			return;
		}

		onRecognize = callback;
		onRecognizeError = errorCallback;
		this.onRecording = onRecording;
		this.onStopedRecording = onStopedRecording;
		onRecordError = onErrorCallback;

		speechRecognition.StartRecord(false);
	}

	private void OnPermissionRequested(bool isGranted)
	{
		//if (isGranted)
		//{
		//	StartCoroutine(WaitForPermissionRequest());
		//	//speechRecognition.SetMicrophoneDevice(Microphone.devices[0]);
		//}
		//else
		//{
		//	if (onRecordError != null)
		//		onRecordError.Invoke("No se cuenta con los permisos para utilizar el micrófono");
		//}
		StartCoroutine(WaitForPermissionRequest());
	}

	IEnumerator WaitForPermissionRequest()
	{
		yield return new WaitForSeconds(0.5f);

		speechRecognition.SetMicrophoneDevice(Microphone.devices[0]);
	}
}
